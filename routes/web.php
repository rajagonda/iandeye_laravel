<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', 'FrontendController@home')->name('site.home');

Route::get('/about-us', 'FrontendController@aboutus')->name('site.aboutus');
Route::match(['GET', 'POST'], '/contact-us', 'FrontendController@contactus')->name('site.contactus');

Route::get('/news-room', 'FrontendController@newsroom')->name('site.news_room');
Route::get('/media/gallery/{album?}', 'FrontendController@mediaGallry')->name('site.media.gallery');
Route::get('/media/videos', 'FrontendController@mediaVidoes')->name('site.media.videos');

Route::match(['GET', 'POST'], '/join-as-volunteer', 'FrontendController@joinAsVolunteer')->name('site.join_as_volunteer');
Route::get('/faq', 'FrontendController@faq')->name('site.faq');

Route::get('/initiative/{alias?}', 'FrontendController@initiative')->name('site.initiative');

//Route::get('/initiative/arogya', 'FrontendController@initiativeArogya')->name('site.initiative.arogya');
//Route::get('/initiative/aahalada', 'FrontendController@initiativeAahalada')->name('site.initiative.aahalada');
Route::get('/locations/{alias?}', 'FrontendController@locations')->name('site.initiative.locations');
Route::get('/projects/{alias?}', 'FrontendController@projects')->name('site.initiative.projects');

Route::get('/events/{name?}', 'FrontendController@events')->name('site.events');
Route::get('/sponsors', 'FrontendController@sponsors')->name('site.sponsors');

Route::get('/financials', 'FrontendController@financialsCurrent')->name('site.financials.current');

Route::match(['GET', 'POST'], '/financials/nextyear', 'FrontendController@financialsNextYear')->name('site.financials.nextyear');

Route::get('/success-stories', 'FrontendController@successStories')->name('site.success_stories');

Route::get('/testimonials', 'FrontendController@testimonials')->name('site.testimonials');

Route::get('/category/{type?}', 'FrontendController@category')->name('site.category');


//Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('admin')->group(function () {

    Route::get('/', 'Auth\LoginController@showLoginForm')->name('route_admin.login');
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('route_admin.login');
    Route::post('/login', 'Auth\LoginController@login')->name('route_admin.login');

    Route::get('/register', 'Auth\RegisterController@showRegisterForm');
    Route::post('/register', 'Auth\RegisterController@register');


    Route::post('/logout', 'Auth\LoginController@logout')->name('route_admin.logout');
    Route::get('/changePassword', 'Admin\AdminController@showChangePasswordForm')->name('changePassword');
    Route::post('/changePassword', 'Admin\AdminController@showChangePasswordForm')->name('changePassword');
    Route::get('/dashboard', 'Admin\AdminController@index')->name('route_admin.dashboard');

    Route::match(['GET', 'POST'], '/sponsers/{action?}', 'Admin\AdminController@sponsers')->name('route_admin.sponsers');
    Route::match(['GET', 'POST'], '/locations', 'Admin\AdminController@locations')->name('route_admin.locations');
    Route::match(['GET', 'POST'], '/projects', 'Admin\AdminController@projects')->name('route_admin.projects');
    Route::match(['GET', 'POST'], '/pages/type', 'Admin\AdminController@pagesTypes')->name('route_admin.pages.type');
    Route::match(['GET', 'POST'], '/pages/list', 'Admin\AdminController@pagesList')->name('route_admin.pages.list');
    Route::match(['GET', 'POST'], '/events', 'Admin\AdminController@events')->name('route_admin.events');

    Route::match(['GET', 'POST'], '/success-stories', 'Admin\AdminController@successStories')->name('route_admin.successStories');
    Route::match(['GET', 'POST'], '/testimonials', 'Admin\AdminController@testimonials')->name('route_admin.testimonials');
    Route::match(['GET', 'POST'], '/financials/activity', 'Admin\AdminController@financialsActivity')->name('route_admin.activity');
    Route::match(['GET', 'POST'], '/financials/activity/yearwise', 'Admin\AdminController@financialsActivityYearWise')->name('route_admin.activity_yearwise');
    Route::match(['GET', 'POST'], '/users', 'Admin\AdminController@users')->name('route_admin.users');
    Route::match(['GET', 'POST'], '/banners', 'Admin\AdminController@banners')->name('route_admin.banners');

    Route::match(['GET', 'POST'], '/contact-us', 'Admin\AdminController@contactUs')->name('route_admin.contactUs');
    Route::match(['GET', 'POST'], '/register-volunteer', 'Admin\AdminController@joinAsVolunteer')->name('route_admin.joinAsVolunteer');
    Route::match(['GET', 'POST'], '/settings', 'Admin\AdminController@settings')->name('route_admin.settings');
    Route::match(['GET', 'POST'], '/media/videos', 'Admin\AdminController@mediaVidoes')->name('route_admin.media.vidoes');
    Route::match(['GET', 'POST'], '/media/press', 'Admin\AdminController@mediaPress')->name('route_admin.media.press');
    Route::match(['GET', 'POST'], '/media/gallery/{album?}', 'Admin\AdminController@mediaGallery')->name('route_admin.media.gallery');

});
Auth::routes();
