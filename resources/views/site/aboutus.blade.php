@extends('layouts.site')

@section('content')

    @if (!empty($pageinfo))

        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
            <div class="container pt-10 pb-10">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">
                                {{ $pageinfo->title }}
                            </h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href={{ route('site.home') }}>Home</a></li>
                                <li class="active text-gray-silver">
                                    {{ $pageinfo->title }}
                                </li>
                                <!--                                <li class="active text-gray-silver">Sahre and Care Initiative</li>-->
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
            <div class="container">
                <div class="section-content">

                    <?php
                    //                dump($pageinfo);
                    ?>

                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="text-theme-color-sky line-bottom">
                            <span class="text-theme-color-red">

                            {{ $pageinfo->title }}

                            </span>
                            </h2>
                            <!--                    <h4 class="text-theme-color-blue">To Society, With Love - Reach out to Recycle</h4>-->
                            <div class="text-justify">

                                {!! $pageinfo->description !!}

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="video-popup">
                                <a>
                                    <img alt="" src="{{ \App\Models\Page::uploadDir('url').'/'.$pageinfo->photo }}"
                                         class="img-responsive img-fullwidth">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

    @else

        <!-- Section: About -->
        <section>
            <div class="container">
                <div class="section-content">

                   <h1 class="text-center mb-5 p-5">
                       404
                   </h1>

                </div>
            </div>

        </section>

    @endif





@endsection


@section('footer_script')

    <script>
        $(function (e) {
            $(".rev_slider_default").revolution({
                sliderType: "standard",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 5000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 800,
                        style: "hebe",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                    }
                },
                responsiveLevels: [1240, 1024, 778],
                visibilityLevels: [1240, 1024, 778],
                gridwidth: [1170, 1024, 778, 480],
                gridheight: [640, 768, 960, 720],
                lazyType: "none",
                parallax: {
                    origo: "slidercenter",
                    speed: 1000,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100,
                        55
                    ],
                    type: "scroll"
                },
                shadow: 2,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        });
    </script>
@endsection
