@extends('layouts.site')

@section('content')

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title text-white">Events</h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li class="active text-gray-silver">Events</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>

    <!-- Section: About -->
    <section>
        <div class="container pt-40 pb-40">
            <div class="row">
                <div class="col-md-8">
                    <h2>
                        {{ $info->name }}
                    </h2>

                    @if ($info->photo != '')

                        <a data-fancybox="gallery"
                           href="{{ \App\Models\Event::uploadDir('url').'/'.$info->photo }}">
                            <img class="img-fullwidth"
                                 src="{{ \App\Models\Event::uploadDir('url').'/'.$info->photo }}"/>
                        </a>



                    @else
                        <img class="img-fullwidth" alt=""
                             src="{{ imageNotAvalableUrl() }}">


                    @endif

                        {{--                    <div class="owl-carousel-1col" data-nav="true">--}}
                        {{--                        <div class="item">--}}
                        {{--                            <img src="/site/images/bg/bg11.jpg" alt="">--}}
                        {{--                        </div>--}}
                        {{--                        <div class="item">--}}
                        {{--                            <img src="/site/images/bg/bg2.jpg" alt="">--}}
                        {{--                        </div>--}}
                        {{--                        <div class="item">--}}
                        {{--                            <img src="/site/images/bg/bg12.jpg" alt="">--}}
                        {{--                        </div>--}}
                        {{--                    </div>--}}


                </div>
                <div class="col-md-4 mt-60">
                    <ul>
                        <li>
                            <h5>Start date:</h5>
                            <p>
                                {{ date('Y-m-d h:i:s A', strtotime($info->event_start_date)) }}
                            </p>
                        </li>
                        <li>
                            <h5>End Date:</h5>
                            <p>
                                {{ date('Y-m-d h:i:s A', strtotime($info->event_end_date)) }}
                            </p>
                        </li>
                        <li>
                            <h5>Location:</h5>
                            <p>
                                {{ $info->location->name }}
                            </p>
                        </li>
                        <li>
                            <h5>
                                Project
                            </h5>
                            <p>
                                {{ $info->project->name }}
                            </p>
                        </li>
                        <li>
                            <h5>
                                Sponsors
                            </h5>
                            <p>
                                {{ implode(', ', $info->sponsors->pluck('name', 'id')->toArray()) }}
                            </p>
                        </li>

                        <li>
                            <h5>Share:</h5>
                            <div class="styled-icons icon-dark icon-theme-color-orange icon-sm icon-circled">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row mt-40">
                <div class="col-md-12">
                    <h3 class="text-theme-color-orange mb-20">Event Description</h3>
                    <div>
                        {{ $info->description }}
                    </div>
                </div>
                {{--                <div class="col-md-6">--}}
                {{--                    <blockquote class="bg-silver-light">--}}
                {{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>--}}
                {{--                        <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>--}}
                {{--                    </blockquote>--}}
                {{--                </div>--}}
            </div>

            @if (count($info->sponsors)> 0)

                <div class="row mt-5">
                    <div class="col-md-12">
                        <h3 class="text-theme-color-orange mb-30">Our Sponsors</h3>
                    </div>
                </div>
                <div class="row multi-row-clearfix">


                    @foreach($info->sponsors AS $sponsor)

                        <div class="col-sm-6 col-md-3 mb-sm-30 sm-text-center">
                            <div class="team maxwidth400">
                                <div class="thumb"><img class="img-fullwidth"
                                                        src="{{ \App\Models\Sponsor::uploadDir('url').'/'.$sponsor->image }}"
                                                        alt=""></div>
                                <div class="content border-1px p-15 bg-theme-color-green clearfix">
                                    <h3 class="name text-white mt-0">
                                        {{ $sponsor->name  }}
                                    </h3>

                                    {{--                                    <ul class="styled-icons icon-dark icon-circled icon-theme-color-orange icon-sm pull-left flip">--}}
                                    {{--                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
                                    {{--                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
                                    {{--                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                                    {{--                                    </ul>--}}

                                </div>
                            </div>
                        </div>

                    @endforeach


                </div>




            @endif


        </div>
    </section>


@endsection


@section('footer_script')


@endsection
