@extends('layouts.site')

@section('content')

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title text-white">Events</h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li class="active text-gray-silver">Events</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>

    <!-- Section: About -->
    <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <section>
                            <div class="container pb-30">
                                <div class="section-content">
                                    <div class="row">

                                        <?php
//                                        dump($lists)
                                        ?>

                                        @if (count($lists)> 0)
                                            @foreach($lists AS $list)


                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="schedule-box maxwidth500 mb-30"
                                                         data-bg-img="/site/images/pattern/p6.png">
                                                        <div class="thumb">


                                                            @if ($list->photo != '')
                                                                <img class="img-fullwidth" alt=""
                                                                     src="{{ \App\Models\Event::uploadDir('url').'/'.$list->photo }}">

                                                               @else
                                                                <img class="img-fullwidth" alt=""
                                                                     src="{{ imageNotAvalableUrl() }}">

                                                            @endif



                                                        </div>
                                                        <div class="schedule-details clearfix p-15 pt-10">
                                                            <div
                                                                class="text-center pull-left flip bg-theme-color-sky p-10 pt-2 pb-2 mr-4">
                                                                <ul>
                                                                    <li class="font-19 text-white font-weight-600 border-bottom ">
                                                                        {{ date('d', strtotime($list->event_start_date)) }}
                                                                    </li>
                                                                    <li class="font-12 text-white text-uppercase">
                                                                        {{ date('M', strtotime($list->event_start_date)) }}
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <h4 class="title font-19">
                                                                <a href="{{ route('site.events', ['name'=>$list->alias]) }}">
                                                                    {{ $list->name }}
                                                                </a>
                                                            </h4>
                                                            <ul class="list-inline font-11 text-black">
                                                                <li>
                                                                    <i class="fa fa-calendar "></i>
                                                                    {{ date('Y M D h:i:s A', strtotime($list->event_start_date)) }}
                                                                </li>
                                                                <li>
                                                                    <i class="fa fa-map-marker "></i>
                                                                    {{ $list->location->name }}
                                                                </li>
                                                            </ul>
                                                            <div class="clearfix"></div>

                                                        </div>
                                                    </div>
                                                </div>



                                            @endforeach
                                        @endif


                                    </div>

                                </div>
                            </div>
                        </section>


                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection


@section('footer_script')

    <script>
        $(function (e) {
            $(".rev_slider_default").revolution({
                sliderType: "standard",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 5000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 800,
                        style: "hebe",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                    }
                },
                responsiveLevels: [1240, 1024, 778],
                visibilityLevels: [1240, 1024, 778],
                gridwidth: [1170, 1024, 778, 480],
                gridheight: [640, 768, 960, 720],
                lazyType: "none",
                parallax: {
                    origo: "slidercenter",
                    speed: 1000,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100,
                        55
                    ],
                    type: "scroll"
                },
                shadow: 2,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        });
    </script>
@endsection
