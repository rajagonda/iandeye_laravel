@extends('layouts.site')

@section('content')

    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title text-white">Conatct Us</h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li class="active text-gray-silver">Conatct Us</li>
                            <!--                                <li class="active text-gray-silver">Sahre and Care Initiative</li>-->
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>

    <!-- Section: container  -->
    <section>
        <!-- Section: Have Any Question -->
        <section class="divider">
            <div class="container pt-60 pb-60">
                <div class="section-title mb-60">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="esc-heading small-border text-center">
                                <h3>Have any Questions?</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="contact-info text-center">
                                <i class="fa fa-phone font-36 mb-10 text-theme-color-red"></i>
                                <h4>Call Us</h4>
                                <h6 class="text-gray">Phone:  {!! settings('conatct_phone') !!}</h6>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="contact-info text-center">
                                <i class="fa fa-map-marker font-36 mb-10 text-theme-color-blue"></i>
                                <h4>Address</h4>
                                <h6 class="text-gray">I & EYE Sharing Vision</h6>
                                <h6 class="text-gray">
                                    {!! settings('conatct_address') !!}
{{--                                    # Flat No.608, Block- C,VSR Celestial Towers,--}}
{{--                                    Gajularamaram,--}}
{{--                                    Qutbhulpur Mandal, Hyderabad, Telangana -55--}}
                                </h6>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="contact-info text-center">
                                <i class="fa fa-envelope font-36 mb-10 text-theme-color-sky"></i>
                                <h4>Email</h4>
                                <h6 class="text-gray">
                                    {!! settings('conatct_email') !!}
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Divider: Contact -->
        <section class="divider bg-lighter">
            <div class="container">
                <div class="row pt-30">
                    <div class="col-md-7">
                        <h3 class="line-bottom mt-0 mb-30">am Interested </h3>


                        <div class="row">
                            <div class="col-md-12">
                                {{ form_flash_message('flash_message') }}
                            </div>
                        </div>


                        <!-- Contact Form -->
                        <form id="contact_validation" name="contact_form" class=""
                              action=""
                              method="post">
                            @csrf

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Name <small>*</small></label>
                                        <input name="form_name" class="form-control" type="text"
                                               placeholder="Enter Name" required="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Email <small>*</small></label>
                                        <input name="form_email" class="form-control required email"
                                               type="email" placeholder="Enter Email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Subject <small>*</small></label>
                                        <input name="form_subject" class="form-control required" type="text"
                                               placeholder="Enter Subject">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input name="form_phone" class="form-control" type="text"
                                               placeholder="Enter Phone">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea name="form_message" class="form-control required" rows="5"
                                          placeholder="Enter Message"></textarea>
                            </div>
{{--                            <div class="form-group">--}}
{{--                                <div class="g-recaptcha"--}}
{{--                                     data-sitekey="6Lf_6d0UAAAAAPoHcO7fDP_3vES7djDKLZ4YIwE2">--}}
{{--                                     data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">--}}
{{--                            </div>--}}
                            <div class="form-group">

                                <button type="submit"
                                        class="btn btn-dark btn-theme-color-sky btn-flat"
                                        data-loading-text="Please wait...">Send your message
                                </button>
                            </div>
                        </form>


                    </div>
                    <div class="col-md-5">

                        <!-- Google Map HTML Codes -->


                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3804.6470996539324!2d78.42273251487826!3d17.524351687996667!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb8e2c11ef24c1%3A0xe70a65075530cbed!2sVSR%20Celestial%20Towers%2C%20Plot%20No%3A%20212%2C213%26214%2C%20Gajularamaram%20Rd%2C%20HAL%20Colony%2C%20Jeedimetla%2C%20Hyderabad%2C%20Telangana%20500055!5e0!3m2!1sen!2sin!4v1582714722377!5m2!1sen!2sin"
                            width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


                    </div>
                </div>
            </div>
        </section>

        <div>
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
    </section>


@endsection


@section('footer_script')

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        $(document).ready(function () {
            $('#contact_validation').validate({
                ignore: [],
                errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    form_name: {
                        required: true
                    },
                    form_email: {
                        required: true,
                        email: true
                    },
                    form_subject: {
                        required: true
                    },
                    form_phone: {
                        required: true,
                        number: true
                    },
                    form_message: {
                        required: true,
                    }
                },
                messages: {
                    form_name: {
                        required: 'Please enter name'
                    },
                    form_email: {
                        required: 'Please enter Email',
                        email: "Enter valid email"
                    },
                    form_subject: {
                        required: 'Please enter Subject'
                    },
                    form_phone: {
                        required: 'Please Enter Contact number',
                        number: 'Please Enter Numbers Only'
                    },
                    form_message: {
                        required: 'Please Enter Message',
                    }
                },
            });
        });
    </script>

@endsection
