@extends('layouts.site')

@section('content')






        <!-- Section: inner-header -->
        <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
            <div class="container pt-10 pb-10">
                <!-- Section Content -->
                <div class="section-content">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="title text-white">
                                Financials Report
                            </h2>
                            <ol class="breadcrumb text-left text-black mt-10">
                                <li><a href={{ route('site.home') }}>Home</a></li>
                                <li>
                                    <a href="#">
                                        Financials
                                    </a>
                                </li>
                                <li class="active text-gray-silver">
                                     Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/ section content -->
            </div>
        </section>

        <!-- Section: About -->
        <section>
            <div class="container">
                <div class="section-content">
                    <div class="row">

                        <?php
//                        dump($list)
                        ?>


                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Years</th>
                                    <th>Description</th>
                                    <th>File</th>



                                </tr>
                                </thead>
                                <tbody>

                                @if (count($lists)> 0)
                                    @foreach($lists AS $list)
                                        <tr>
                                            <th scope="row">
                                                {{ $loop->iteration }}
                                            </th>
                                            <td>
                                                {{ $list->financial_year }}
                                            </td>

                                            <td>
                                                {!! $list->desc !!}
                                            </td>

                                            <td>

                                                <a class="btn btn-primary" href=" {{ \App\Models\FinancialYear::uploadDir('url').'/'.$list->file_name }}"
                                                   download="{{ $list->financial_year.'_file_'.$list->file_name }}">
                                                    Download
                                                </a>

                                            </td>




                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>


{{--                        <div class="col-md-12 text-center">--}}
{{--                            <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->--}}
{{--                            <h4>--}}
{{--                                {{ $pageinfo->title }}--}}
{{--                            </h4>--}}

{{--                            <img alt="" src="{{ \App\Models\Page::uploadDir('url').'/'.$pageinfo->photo }}"--}}
{{--                                 class="img-responsive img-fullwidth">--}}


{{--                            --}}{{--                        <img src="/site/images/current_finance.jpg" class="">--}}


{{--                        </div>--}}
                    </div>
                </div>
            </div>

        </section>






@endsection


@section('footer_script')


@endsection
