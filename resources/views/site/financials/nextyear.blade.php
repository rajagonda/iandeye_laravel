@extends('layouts.site')

@section('content')

    <!-- Section: inner-header -->
    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title text-white"> Next Year budget activity wise</h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li><a href="#">Financials</a></li>
                            <li class="active text-gray-silver"> Next Year budget activity wise</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>



    <!-- Section: About -->
    <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <h4>
                            Next Year budget activity wise
                        </h4>

                        <div class="row">
                            <div class="col-md-12">
                                {{ form_flash_message('flash_message') }}
                            </div>
                        </div>

                        <?php
//                        dump($lists)
                        ?>


                        <table class="table table-bordered table-striped">

                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Period</th>
                                <th>No Of Beneficiaries</th>
                                <th>Budget</th>
                                <th>Interests</th>
                                <th>Status</th>
                                <th>action</th>
                            </tr>
                            </thead>
                            <tbody>

                            @if (count($lists)> 0)
                                @foreach($lists AS $list)

                                    <tr>
                                        <td>
                                            <a href="javascript:void(0)">
                                                {{ $list->activity_name }}

                                            </a>
                                        </td>
                                        <td>
                                            {{ $list->period }}
                                        </td>
                                        <td>
                                            {{ $list->no_of_beneficiaries }}
                                        </td>
                                        <td>
                                            {{ $list->project_cost }}
                                        </td>
                                        <td>

                                        {{  $list->interests->count() }}

                                        </td>
                                        <td>
                                         <span class="label label-success">
                                            {{ activity_list($list->status) }}
                                        </span>
                                        </td>
                                        <td>


                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#intrest_{{ $list->id }}">
                                                <i class="fa fa-thumbs-up" aria-hidden="true"></i>

                                                Interest
                                            </button>

                                            <!-- The Modal -->
                                            <div class="modal" id="intrest_{{ $list->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">

                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">

                                                                Interest on test name

                                                            </h4>
                                                            <button type="button" class="close" data-dismiss="modal">
                                                                &times;
                                                            </button>
                                                        </div>

                                                        <!-- Modal body -->
                                                        <div class="modal-body">


                                                            <form id="donation_frm" action="" method="post">
                                                                @csrf
                                                                <input type="hidden" name="activity_id"
                                                                       value="{{ $list->id }}"/>

                                                                <input type="hidden" name="type"
                                                                       value="add_interest"/>

                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label for="exampleFullName"
                                                                               class="control-label">Full
                                                                            Name</label>
                                                                        <input type="text" class="form-control"
                                                                               id="exampleFullName"
                                                                               name="name"
                                                                               aria-describedby="emailHelp"
                                                                               placeholder="Full Name">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Email
                                                                            address</label>
                                                                        <input type="email" class="form-control"
                                                                               id="exampleInputEmail1"
                                                                               name="email"
                                                                               aria-describedby="emailHelp"
                                                                               placeholder="Enter email">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="examplePhoneNumber">Phone
                                                                            Number</label>
                                                                        <input type="number" class="form-control"
                                                                               id="examplePhoneNumber"
                                                                               name="phone"
                                                                               placeholder="Phone Number">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="examplePrice">Price</label>
                                                                        <input type="number" class="form-control"
                                                                               id="examplePrice" name="price"
                                                                               placeholder="Price">

                                                                    </div>

                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default"
                                                                            data-dismiss="modal">Close
                                                                    </button>
                                                                    <input type="submit" class="btn btn-primary"
                                                                           id="btn_submit_id" name="btn_submit_id"
                                                                           value="Submit">
                                                                </div>
                                                            </form>


                                                        </div>

                                                        <!-- Modal footer -->
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger"
                                                                    data-dismiss="modal">Close
                                                            </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>


                                @endforeach
                            @endif




                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
        <div>
            <img alt="" src="images/bg/f2.png" class="img-responsive img-fullwidth">
        </div>
    </section>




@endsection


@section('footer_script')


@endsection
