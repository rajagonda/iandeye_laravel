@extends('layouts.site')

@section('content')

    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title text-white">

                            Join As Volunteer

                        </h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li class="active text-gray-silver">
                                Join As Volunteer
                            </li>
                            <!--                                <li class="active text-gray-silver">Sahre and Care Initiative</li>-->
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>

    <!-- Section: container  -->
    <section>
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col 12-->
                <div class="col-lg-12">
                    <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join As </span>
                        Volunteer
                    </h2>
                    <div class="row">
                        <div class="col-md-12">
                            {{ form_flash_message('flash_message') }}
                        </div>
                    </div>

                    <form id="contact_validation" name="contact_form" class=""
                          action=""
                          method="post">
                        @csrf

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Name <small>*</small></label>
                                    <input name="form_name" class="form-control" type="text"
                                           placeholder="Enter Name" required="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Email <small>*</small></label>
                                    <input name="form_email" class="form-control required email"
                                           type="email" placeholder="Enter Email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Subject <small>*</small></label>
                                    <input name="form_subject" class="form-control required" type="text"
                                           placeholder="Enter Subject">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input name="form_phone" class="form-control" type="text"
                                           placeholder="Enter Phone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Message</label>
                            <textarea name="form_message" class="form-control required" rows="5"
                                      placeholder="Enter Message"></textarea>
                        </div>
                        {{--                            <div class="form-group">--}}
                        {{--                                <div class="g-recaptcha"--}}
                        {{--                                     data-sitekey="6Lf_6d0UAAAAAPoHcO7fDP_3vES7djDKLZ4YIwE2">--}}
                        {{--                                     data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">--}}
                        {{--                            </div>--}}
                        <div class="form-group">

                            <button type="submit"
                                    class="btn btn-dark btn-theme-color-sky btn-flat"
                                    data-loading-text="Please wait...">Send your message
                            </button>
                        </div>
                    </form>


                </div>
                <!--/ col 12-->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->


    </section>

@endsection



@section('footer_script')

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        $(document).ready(function () {
            $('#contact_validation').validate({
                ignore: [],
                errorClass: 'help-block animation-slideDown text-danger', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    form_name: {
                        required: true
                    },
                    form_email: {
                        required: true,
                        email: true
                    },
                    form_subject: {
                        required: true
                    },
                    form_phone: {
                        required: true,
                        number: true
                    },
                    form_message: {
                        required: true,
                    }
                },
                messages: {
                    form_name: {
                        required: 'Please enter name'
                    },
                    form_email: {
                        required: 'Please enter Email',
                        email: "Enter valid email"
                    },
                    form_subject: {
                        required: 'Please enter Subject'
                    },
                    form_phone: {
                        required: 'Please Enter Contact number',
                        number: 'Please Enter Numbers Only'
                    },
                    form_message: {
                        required: 'Please Enter Message',
                    }
                },
            });
        });
    </script>

@endsection
