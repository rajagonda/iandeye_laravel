@extends('layouts.site')

@section('content')

    <!-- Section: home -->
    <section id="home">
        <div class="container-fluid p-0">


        @if (count($banners) > 0)

            <!-- Slider Revolution Start -->
                <div class="rev_slider_wrapper">
                    <div class="rev_slider rev_slider_default" data-version="5.0">
                        <ul>


                            @foreach($banners AS $banner)

                                @if ($banner->photo != '')

                                    @php
                                        $bannerimage = \App\Models\Banner::uploadDir('url').'/'.$banner->photo;
                                    @endphp



                                @else

                                    @php
                                        $bannerimage ='/site/images/bg/bg1.jpg';
                                    @endphp



                                @endif

                                <li data-index="rs-{{ $loop->iteration }}" data-transition="fade"
                                    data-slotamount="default"
                                    data-easein="default" data-easeout="default" data-masterspeed="default"
                                    data-thumb="{{ $bannerimage }}" data-rotate="0" data-fstransition="fade"
                                    data-saveperformance="off" data-title="Web Show" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{ $bannerimage }}" alt="" data-bgposition="center center"
                                         data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg"
                                         data-bgparallax="6" data-no-retina>
                                    <!-- LAYERS -->

                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme rs-parallaxlevel-0"
                                         id="slide-1-layer-1" data-x="['center','center','center','center']"
                                         data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                                         data-voffset="['0','0','0','0']" data-width="full" data-height="full"
                                         data-whitespace="normal" data-transform_idle="o:1;"
                                         data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                         data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                         data-start="1000" data-basealign="slide" data-responsive_offset="on"
                                         style="z-index: 5;background-color:rgba(0, 0, 0, 0.05);border-color:rgba(0, 0, 0, 1.00);">
                                    </div>

                                    <div class="banner-text">
                                        <!-- LAYER NR. 2 -->
                                        <div
                                            class="tp-caption tp-resizeme sft font-Lobster font-weight-700 text-theme-color-red"
                                            id="rs-1-layer-2" data-x="['left']" data-hoffset="['30']"
                                            data-y="['middle']"
                                            data-voffset="['-115']" data-fontsize="['28']" data-lineheight="['48']"
                                            data-width="none" data-height="none" data-whitespace="nowrap"
                                            data-transform_idle="o:1;s:500"
                                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-speed="500"
                                            data-start="600" data-splitin="none" data-splitout="none"
                                            data-responsive_offset="on" style="z-index: 5; white-space: nowrap;">
                                            {{ $banner->title }}
                                        </div>

                                        <!-- LAYER NR. 3 -->
                                        <div
                                            class="tp-caption tp-resizeme sft text-theme-color-blue font-Lobster font-weight-700 m-0"
                                            id="rs-1-layer-3" data-x="['left']" data-hoffset="['30']"
                                            data-y="['middle']"
                                            data-voffset="['-50']" data-fontsize="['78']" data-lineheight="['96']"
                                            data-width="none" data-height="none" data-whitespace="nowrap"
                                            data-transform_idle="o:1;s:500"
                                            data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                            data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                            data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                            data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-speed="500"
                                            data-start="800" data-splitin="none" data-splitout="none"
                                            data-responsive_offset="on" style="z-index: 5; white-space: nowrap;">

                                            {{ $banner->sub_title }}

                                            {{--                                            Kinder--}}
                                            {{--                                            <span class="text-theme-color-red">Garten</span>--}}
                                        </div>

                                        <!-- LAYER NR. 4 -->
                                        <div class="tp-caption tp-resizeme sft text-black" id="rs-1-layer-4"
                                             data-x="['left']" data-hoffset="['30']" data-y="['middle']"
                                             data-voffset="['20']" data-fontsize="['16','18',24']"
                                             data-lineheight="['28']"
                                             data-width="none" data-height="none" data-whitespace="nowrap"
                                             data-transform_idle="o:1;s:500"
                                             data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                                             data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" data-speed="500"
                                             data-start="1200" data-splitin="none" data-splitout="none"
                                             data-responsive_offset="on"
                                             style="z-index: 5; white-space: nowrap; font-weight:500;">

                                            {!! $banner->desc !!}

                                        </div>

                                    </div>


                                </li>


                            @endforeach

                        </ul>
                    </div>
                    <!-- end .rev_slider -->
                </div>
                <!-- end .rev_slider_wrapper -->

        @endif



        <!-- Slider Revolution Ends -->
        </div>
    </section>

<br />
<br />

{{--    @if (count($pageTypes)> 0)--}}


{{--        <section class="divider" data-bg-img="/site/images/bg/b1.png" data-margin-top="-34px">--}}
{{--            <div class="container">--}}
{{--                <div class="section-content">--}}
{{--                    <div class="row">--}}

{{--                        @foreach($pageTypes AS $pageType)--}}

{{--                            <div class="col-sm-6 col-md-4">--}}
{{--                                <div class="sm-height-auto">--}}
{{--                                    <div class="text-center pb-sm-20">--}}
{{--                                        <div class="icon-box text-center">--}}
{{--                                            <a class="mb-5" href="#">--}}


{{--                                                @if ($pageType->photo!='')--}}
{{--                                                    <img class="img-thumbnail"--}}
{{--                                                         src="{{ \App\Models\Page::uploadDir('url').'/'.$pageType->photo }}"/>--}}
{{--                                                @else--}}

{{--                                                    <img class="img-thumbnail"--}}
{{--                                                         src="{{ imageNotAvalableUrl() }}"/>--}}
{{--                                                @endif--}}


{{--                                            </a>--}}

{{--                                            <h4 class="">--}}
{{--                                                {{ $pageType->title }}--}}
{{--                                            </h4>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}


{{--                        @endforeach--}}



{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </section>--}}


{{--    @endif--}}



    <!-- Section: About -->
    <section>
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-6">


                        {!! settings('home_about') !!}


{{--                        <h2 class="text-theme-color-sky line-bottom">Donate Blood <span--}}
{{--                                class="text-theme-color-red">Save Lives</span></h2>--}}
{{--                        <p class="text-theme-color-blue">NO MATTER WHERE YOU ARE IN THE WORLD, IF YOU HAVE--}}
{{--                            DECIDED TO DO SOMETHING DEEP FROM YOUR HEART, YOU CAN DO IT. IT IS THE THOUGHT THAT--}}
{{--                            MATTERS NOT WHERE YOU ARE OR WHERE THE PERSON IS.</p>--}}
{{--                        <p> The main intention of this group is to instigate and rekindle the spirit of service--}}
{{--                            in every citizen. We can continue with our day-to-day life and still can work for--}}
{{--                            positive and progressive changes in our society. Encourage people to form as groups--}}
{{--                            with friends and like minded people and work towards attainable goals.</p>--}}
{{--                        <a href="#" class="btn btn-flat btn-colored btn-theme-color-blue mt-15 mr-15">Read--}}
{{--                            More</a>--}}
                    </div>
                    <div class="col-md-6">
                        <div class="video-popup">
                            <a>
                                <img alt="" src="/site/images/about/home_about.jpg" class="img-responsive img-fullwidth">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    @if ($gallery_lists->count()>0)
    <!-- Section: Our Gallery -->
    <section>
        <div class="container pb-70">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="line-bottom-center mt-0">Our <span
                                class="text-theme-color-red">Gallery</span></h2>
                        <div class="title-flaticon">
                            <i class="flaticon-charity-alms"></i>
                        </div>

                    </div>
                </div>
            </div>
            <div class="section-content">
                <div class="row mb-30">

                    <?php
                    //                            dump($lists)
                    ?>


                        @foreach($gallery_lists AS $list)


                            @if ($list->galleryImages->count()> 0)
                                @php
                                    $galleryimage = $list->galleryImages->first();
                                $galleryimageUrl = \App\Models\MediaGalleryImages::uploadDir('url').'/'.$galleryimage->image_name;
                                @endphp
                                <?php
                                //                                        dump($galleryimage)
                                ?>
                            @endif

                            <div class="col-md-3">

                                <div class="card">
                                    <div class="card-body">
                                        <a href="{{ route('site.media.gallery', ['album'=>$list->album_name]) }}">
                                            <img src="{{$galleryimageUrl}}"/>
                                        </a>

                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('site.media.gallery', ['album'=>$list->album_name]) }}">

                                            {{ $list->album_name }}
                                        </a>
                                    </div>

                                </div>


                            </div>

                        @endforeach




                </div>

            </div>
        </div>
    </section>

    @endif
@endsection


@section('footer_script')

    <script>
        $(function (e) {
            $(".rev_slider_default").revolution({
                sliderType: "standard",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 5000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 0,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 800,
                        style: "hebe",
                        hide_onleave: false,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title"></span>'
                    }
                },
                responsiveLevels: [1240, 1024, 778],
                visibilityLevels: [1240, 1024, 778],
                gridwidth: [1170, 1024, 778, 480],
                gridheight: [640, 768, 960, 720],
                lazyType: "none",
                parallax: {
                    origo: "slidercenter",
                    speed: 1000,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100,
                        55
                    ],
                    type: "scroll"
                },
                shadow: 2,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        });
    </script>
@endsection
