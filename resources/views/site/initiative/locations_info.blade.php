@extends('layouts.site')

@section('content')

    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">


                        <h2 class="title text-white">
                            {{ $info->name }}
                        </h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li><a href={{ route('site.initiative.locations') }}>locations</a></li>
                            <li class="active text-gray-silver">
                                {{ $info->name  }}
                            </li>
                            <!--                                <li class="active text-gray-silver">Sahre and Care Initiative</li>-->
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>


    <section>
        <div class="container pt-40 pb-40">
            <div class="row">
                <div class="col-md-6">


                    <h2>
                        {{ $info->name  }}
                    </h2>


                    @if ($info->photo != '')

                        <img alt=""
                             src="{{ \App\Models\Location::uploadDir('url').'/'.$info->photo }}"
                             class="img-responsive img-fullwidth">

                    @else

                        <img alt="" src="{{ imageNotAvalableUrl() }}"
                             class="img-fullwidth">

                    @endif


                </div>
                <div class="col-md-6 mt-60">
                    <ul>

                        <li>
                            <h5>Location:</h5>
                            <p>{{ $info->description }}</p>
                        </li>

                        <li>
                            <h5>Share:</h5>
                            <div class="styled-icons icon-dark icon-theme-color-orange icon-sm icon-circled">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            {{--            <div class="row mt-40">--}}
            {{--                <div class="col-md-6">--}}
            {{--                    <h3 class="text-theme-color-orange mb-20">Event Description</h3>--}}
            {{--                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi id perspiciatis facilis--}}
            {{--                        nulla possimus quasi, amet qui. Ea rerum officia, aspernatur nulla neque nesciunt alias--}}
            {{--                        repudiandae doloremque, dolor, quam nostrum laudantium earum illum odio quasi excepturi--}}
            {{--                        mollitia corporis quas ipsa modi nihil, ad ex tempore.</p>--}}
            {{--                </div>--}}
            {{--                <div class="col-md-6">--}}
            {{--                    <blockquote class="bg-silver-light">--}}
            {{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>--}}
            {{--                        <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>--}}
            {{--                    </blockquote>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="row mt-5">
                <div class="col-md-12">
                    <h3 class="text-theme-color-orange mb-30"> Projects </h3>
                </div>
            </div>
            <div class="row multi-row-clearfix">

                <?php
//                dd($info->projects)
                ?>

                @if (count($info->projects) > 0)
                    @foreach($info->projects AS $project)

                        <div class="col-sm-6 col-md-3 mb-sm-30 sm-text-center">
                            <div class="team maxwidth400">
                                <div class="thumb">

                                    @if ($project->photo != '')

                                        <a data-fancybox="gallery"
                                           href="{{ \App\Models\Project::uploadDir('url').'/'.$project->photo }}">
                                            <img class="img-fullwidth"
                                                 src="{{ \App\Models\Project::uploadDir('url').'/'.$project->photo }}"/>
                                        </a>

                                    @else
                                        <a href="">
                                            <img class="img-fullwidth"
                                                 src="{{ imageNotAvalableUrl() }}"/>
                                        </a>
                                    @endif


                                </div>

                                <?php
                                //                                dump($project);
                                ?>

                                <div class="content border-1px p-15 bg-theme-color-green clearfix">
                                    <h3 class="name text-white mt-0">
                                        <a href="">
                                                                                        {{ $project->name }}
                                        </a>

                                    </h3>
{{--                                    <p class="text-white mb-20">--}}
{{--                                        Lorem ipsum dolor sit amet, con amit sectetur--}}
{{--                                        adipisicing--}}
{{--                                        elit.--}}
{{--                                    </p>--}}


                                </div>
                            </div>
                        </div>


                    @endforeach
                @endif


            </div>
        </div>
    </section>





@endsection


@section('footer_script')


@endsection
