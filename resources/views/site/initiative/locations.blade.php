@extends('layouts.site')

@section('content')

    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title text-white">locations</h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li class="active text-gray-silver">locations</li>
                            <!--                                <li class="active text-gray-silver">Sahre and Care Initiative</li>-->
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>

    <!-- Section: About -->
    <section>
        <div class="container">
            <div class="section-content">
                <div class="row">


                    <div class="col-md-12 pricing-table">
                        <!-- <h2 class="text-theme-color-sky line-bottom"><span class="text-theme-color-red">Join</span>Us</h2> -->

                        <section>
                            <div class="container pb-30">
                                <div class="section-content">
                                    <div class="row">


                                        @if (count($lists)> 0)

                                            @foreach($lists AS $list)

                                                <div class="col-sm-4 col-md-4 col-lg-4">
                                                    <div class="schedule-box maxwidth500 mb-30"
                                                         data-bg-img="/site/images/pattern/p6.png">
                                                        <div class="thumb">

                                                            @if ($list->photo != '')

                                                                <img alt=""
                                                                     src="{{ \App\Models\Location::uploadDir('url').'/'.$list->photo }}"
                                                                     class="img-responsive img-fullwidth">

                                                            @else

                                                                <img alt="" src="{{ imageNotAvalableUrl() }}"
                                                                     class="img-fullwidth">

                                                            @endif


                                                        </div>
                                                        <div class="schedule-details clearfix p-15 pt-10">

                                                            <h4 class="title font-19">
                                                                <a href="{{ route('site.initiative.locations', ['alias'=>$list->alias]) }}">
                                                                    {{ $list->name }}
                                                                </a>
                                                            </h4>
                                                            <ul class="list-inline font-11 text-black">

                                                                <li><i class="fa fa-map-marker "></i>
                                                                    {{ $list->description }}
                                                                </li>
                                                            </ul>
                                                            <div class="clearfix"></div>

                                                        </div>
                                                    </div>
                                                </div>



                                            @endforeach

                                        @endif


                                    </div>

                                </div>
                            </div>
                        </section>


                    </div>


                </div>
            </div>
        </div>

    </section>


@endsection


@section('footer_script')


@endsection
