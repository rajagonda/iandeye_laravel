@extends('layouts.site')

@section('content')

    <section class="inner-header divider parallax layer-overlay overlay-dark-5" data-bg-img="/site/images/bg/bg3.jpg">
        <div class="container pt-10 pb-10">
            <!-- Section Content -->
            <div class="section-content">
                <div class="row">
                    <div class="col-md-12">

                        <?php
                        //                        dd($info);
                        ?>


                        <h2 class="title text-white">
                            {{ $info->name }}
                        </h2>
                        <ol class="breadcrumb text-left text-black mt-10">
                            <li><a href={{ route('site.home') }}>Home</a></li>
                            <li><a href={{ route('site.initiative.locations') }}>Projects</a></li>
                            <li class="active text-gray-silver">
                                {{ $info->name  }}
                            </li>
                            <!--                                <li class="active text-gray-silver">Sahre and Care Initiative</li>-->
                        </ol>
                    </div>
                </div>
            </div>
            <!--/ section content -->
        </div>
    </section>


    <section>
        <div class="container pt-40 pb-40">
            <div class="row">
                <div class="col-md-6">


                    <h2>
                        {{ $info->name  }}
                    </h2>


                    @if ($info->photo != '')

                        <img alt=""
                             src="{{ \App\Models\Project::uploadDir('url').'/'.$info->photo }}"
                             class="img-responsive img-fullwidth">

                    @else

                        <img alt="" src="{{ imageNotAvalableUrl() }}"
                             class="img-fullwidth">

                    @endif


                </div>
                <div class="col-md-6 mt-60">


                    <h3 class="text-theme-color-orange mb-20">Project Description</h3>
                    <p>
                        {{ $info->desc }}
                    </p>
{{--                    <ul>--}}

{{--                        <li>--}}
{{--                            <h5>Location:</h5>--}}
{{--                            <p>{{ $info->description }}</p>--}}
{{--                        </li>--}}

{{--                        <li>--}}
{{--                            <h5>Share:</h5>--}}
{{--                            <div class="styled-icons icon-dark icon-theme-color-orange icon-sm icon-circled">--}}
{{--                                <a href="#"><i class="fa fa-facebook"></i></a>--}}
{{--                                <a href="#"><i class="fa fa-twitter"></i></a>--}}
{{--                                <a href="#"><i class="fa fa-instagram"></i></a>--}}
{{--                                <a href="#"><i class="fa fa-google-plus"></i></a>--}}
{{--                            </div>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
                </div>
            </div>


        </div>
    </section>




@endsection


@section('footer_script')


@endsection
