<!-- Footer -->
<footer id="footer" class="footer divider layer-overlay overlay-dark-9" >

    <div class="footer-bottom bg-black-333">
        <div class="container pt-20 pb-20">
            <div class="row">
                <div class="col-md-3">
                    <p class="font-11 text-black-777 m-0">Copyright &copy;2020 I & EYE. All Rights Reserved
                    </p>
                </div>
{{--                <div class="col-md-9 text-right">--}}
{{--                    <div class="widget no-border m-0">--}}
{{--                        <ul class="list-inline sm-text-center font-12">--}}

{{--                            <li class="{{ (isset($active) && $active== 'faq' )?'active':'' }}">--}}
{{--                                <a href="{{ route('site.faq') }}">--}}
{{--                                    FAQ--}}
{{--                                </a>--}}
{{--                            </li>--}}


{{--                            <li>|</li>--}}
{{--                            --}}{{--                            <li>--}}
{{--                            --}}{{--                                <a href="#">JOIN AS VOLUNTEER</a>--}}
{{--                            --}}{{--                            </li>--}}


{{--                            <li class="{{ (isset($active) && $active== 'join_as_volunteer' )?'active':'' }}">--}}
{{--                                <a href="{{ route('site.join_as_volunteer') }}">--}}
{{--                                    JOIN AS VOLUNTEER--}}

{{--                                </a>--}}
{{--                            </li>--}}

{{--                            <li>|</li>--}}

{{--                            <li class="{{ (isset($active) && $active== 'news_room' )?'active':'' }}">--}}
{{--                                <a href="{{ route('site.news_room') }}">News Room</a>--}}
{{--                            </li>--}}

{{--                            <li>|</li>--}}

{{--                            <li class="{{ (isset($active) && $active== 'mediaGallry' )?'active':'' }}">--}}
{{--                                <a href="{{ route('site.media.gallery') }}">Gallery</a>--}}
{{--                            </li>--}}


{{--                            <li>|</li>--}}

{{--                            <li class="{{ (isset($active) && $active== 'mediaVidoes' )?'active':'' }}">--}}
{{--                                <a href="{{ route('site.media.videos') }}">Videos</a>--}}
{{--                            </li>--}}

{{--                            <li>|</li>--}}




{{--                            <li class="{{ (isset($active) && $active== 'contactus' )?'active':'' }}">--}}
{{--                                <a href="{{ route('site.contactus') }}"> Contact Us</a>--}}
{{--                            </li>--}}


{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>
</footer>
<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
