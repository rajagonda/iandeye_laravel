<header id="header" class="header">
    <!-- header top -->
{{--    <div class="header-top top-header sm-text-center p-0">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-md-9">--}}
{{--                    <div class="widget no-border m-0">--}}
{{--                        <ul class="list-inline font-13 sm-text-center mt-3">--}}

{{--                            <!--                            <li>-->--}}
{{--                            <!--                                <a class="text-white" href="#">-->--}}
{{--                            <!--                                    FAQ-->--}}
{{--                            <!--                                </a>-->--}}
{{--                            <!--                            </li>-->--}}
{{--                            <!--                            <li class="text-white">|</li>-->--}}
{{--                            <!--                            <li>-->--}}
{{--                            <!--                                <a class="text-white" href="#">-->--}}
{{--                            <!--                                    Help Desk-->--}}
{{--                            <!--                                </a>-->--}}
{{--                            <!--                            </li>-->--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-5"></div>--}}
{{--                <div class="col-md-4">--}}


{{--                    <div class="row">--}}

{{--                        <div class="col-md-12 mb-2 text-right">--}}
{{--                            <i class="fa fa-phone-square  text-white font-20 mt-2 "></i>--}}
{{--                            <a href="#" class="font-12  text-white text-uppercase">--}}
{{--                                Call us today!--}}
{{--                            </a>--}}
{{--                            <span class="font-14  text-white m-0">--}}
{{--                                --}}{{--                                    +91 944 000 1050--}}
{{--                                {!! settings('conatct_phone') !!}--}}
{{--                            </span>--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--                <div class="col-md-3">--}}
{{--                    <div class="widget no-border m-0 mr-15 pull-right flip sm-pull-none sm-text-center">--}}
{{--                        <ul class="styled-icons icon-circled icon-sm pull-right flip sm-pull-none sm-text-center mt-sm-15">--}}
{{--                            <li>--}}
{{--                                <a href="https://www.facebook.com/iandeye.sharingvision">--}}
{{--                                    <i class="fa fa-facebook text-white"></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--                                    <i class="fa fa-twitter text-white"></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--                                    <i class="fa fa-google-plus text-white"></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--                                    <i class="fa fa-instagram text-white"></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#">--}}
{{--                                    <i class="fa fa-linkedin text-white"></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
<!--/ header top -->


    <!-- header middle -->
    <div class="header-middle p-0 bg-lightest xs-text-center">
        <div class="container pt-0 pb-0">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-5">
                    <div class="widget no-border m-0">
                        <a class="menuzord-brand pull-left flip xs-pull-center mb-15" href="{{ route('site.home') }}">
                            <img src="/site/images/logo-wide.png" alt="I & EYE">
                        </a>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-4 col-md-3">
                    <!--                    <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">-->
                    <!--                        <ul class="list-inline">-->
                    <!--                            <li>-->
                    <!--                                <i class="fa fa-clock-o text-theme-color-red font-36 mt-5 sm-display-block"></i>-->
                    <!--                            </li>-->
                    <!--                            <li>-->
                    <!--                                <a href="#" class="font-12 text-black text-uppercase">-->
                    <!--                                    We are open!-->
                    <!--                                </a>-->
                    <!--                                <h5 class="font-13 text-theme-color-blue m-0">-->
                    <!--                                    Mon-Fri 8:00-16:00-->
                    <!--                                </h5>-->
                    <!--                            </li>-->
                    <!--                        </ul>-->
                    <!--                    </div>-->
                </div>
                {{--                <div class="col-xs-12 col-sm-4 col-md-4">--}}
                {{--                    <div class="widget no-border pull-right sm-pull-none sm-text-center mt-10 mb-10 m-0">--}}
                {{--                        <ul class="list-inline">--}}
                {{--                            <li>--}}
                {{--                                <i class="fa fa-phone-square text-theme-color-sky font-36 mt-3 sm-display-block"></i>--}}
                {{--                            </li>--}}
                {{--                            <li>--}}
                {{--                                <a href="#" class="font-12 text-black text-uppercase">--}}
                {{--                                    Call us today!--}}
                {{--                                </a>--}}
                {{--                                <h5 class="font-14 text-theme-color-blue m-0">--}}
                {{--                                    --}}{{--                                    +91 944 000 1050--}}
                {{--                                    {!! settings('conatct_phone') !!}--}}
                {{--                                </h5>--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </div>--}}

            </div>
        </div>
    </div>
    <!-- header middle -->

    <!-- header main -->
    <div class="header-nav">
        <div class="header-nav-wrapper navbar-scrolltofixed bg-theme-color-red border-bottom-theme-color-sky-2px">
            <div class="container">
                <nav id="menuzord" class="menuzord bg-theme-colored pull-left flip menuzord-responsive">
                    <ul class="menuzord-menu">
                        <li class="{{ (isset($active) && $active== 'home')?'active':'' }}">
                            <a href="{{ route('site.home') }}">
                                Home
                            </a>
                        </li>
                        <li class="{{ (isset($active) && ($active== 'aboutus' || $active== 'events' || $active== 'sponsors' || $active== 'financials') )?'active':'' }}">
                            <a href="{{ route('site.aboutus') }}">
                                About us


                                {{--                                {{ (isset($active) && ($active== 'aboutus' || $active== 'events' || $active== 'sponsors' || $active== 'financials') )?'active':'ddd' }}--}}

                            </a>

                            <ul class="dropdown">
                                <li class="{{ (isset($active) && $active== 'events' )?'active':'' }}">
                                    <a href="{{ route('site.events') }}">Events</a>
                                </li>
                                <li class="{{ (isset($active) && $active== 'sponsors' )?'active':'' }}">
                                    <a href="{{ route('site.sponsors') }}">Sponsors</a>
                                </li>

                                <li class="{{ (isset($active) && $active== 'financials' )?'active':'' }}">
                                    <a href="{{ route('site.financials.current') }}">
                                        Financials
                                    </a>

                                    {{--                                    <ul class="dropdown">--}}
                                    {{--                                        <li class="{{ (isset($active) && $active== 'financials' && $sub1_active== 'current')?'active':'' }}">--}}
                                    {{--                                            <a href="{{ route('site.financials.current') }}">--}}
                                    {{--                                                FY Income & Expenditure--}}
                                    {{--                                            </a>--}}
                                    {{--                                        </li>--}}
                                    {{--                                        --}}{{--                                <li class="{{ (isset($active) && $active== 'financials' && $sub1_active== 'nextyear')?'active':'' }}">--}}
                                    {{--                                        --}}{{--                                    <a href="{{ route('site.financials.nextyear') }}">--}}
                                    {{--                                        --}}{{--                                        Next Year budget activity wise--}}
                                    {{--                                        --}}{{--                                    </a>--}}
                                    {{--                                        --}}{{--                                </li>--}}
                                    {{--                                    </ul>--}}
                                </li>

                            </ul>
                        </li>

                        <li class="{{ (isset($active) && $active== 'initiative')?'active':'' }}">
                            <a href="#">Initiative</a>
                            <ul class="dropdown">

                                @php
                                    $initiativeLists = initiativeList();
                                @endphp

                                @if (count($initiativeLists)> 0)
                                    @foreach($initiativeLists AS $initiativeList )
                                        <li class="{{ (isset($active) && $active== $initiativeList->pageTypes->name && isset($sub1_active) && $sub1_active== $initiativeList->alias)?'active':'' }}">
                                            <a href="{{ route('site.initiative', ['alias'=>$initiativeList->alias]) }}">
                                                {{ $initiativeList->title }}
                                            </a>
                                        </li>
                                    @endforeach

                                @endif

{{--                                <li class="{{ (isset($sub1_active) && $sub1_active== 'success_stories' )?'active':'' }}">--}}
{{--                                    <a href="{{ route('site.success_stories') }}">Success Stories</a>--}}
{{--                                </li>--}}

{{--                                <li class="{{ (isset($sub1_active) && $sub1_active== 'testimonials' )?'active':'' }}">--}}
{{--                                    <a href="{{ route('site.testimonials') }}">Testimonials</a>--}}
{{--                                </li>--}}


                            </ul>
                        </li>

{{--                        <li class="{{ (isset($active) && $active== 'locations' )?'active':'' }}">--}}
{{--                            <a href="{{ route('site.initiative.locations') }}">--}}
{{--                                Locations--}}
{{--                            </a>--}}
{{--                        </li>--}}

                        <li class="{{ (isset($active) && $active== 'projects' )?'active':'' }}">
                            <a href="{{ route('site.initiative.projects') }}">
                                Projects
                            </a>
                        </li>

                        <li class="{{ (isset($active) && $active== 'contactus' )?'active':'' }}">
                            <a href="{{ route('site.contactus') }}"> Contact Us</a>
                        </li>


                        {{--                        <li class="{{ (isset($active) && $active== 'news' )?'active':'' }}">--}}
                        {{--                            <a href="{{ route('site.sponsors') }}">news</a>--}}
                        {{--                        </li>--}}

                        {{--                        <li class="{{ (isset($active) && $active== 'news' )?'active':'' }}">--}}
                        {{--                            <a href="{{ route('site.sponsors') }}">Media</a>--}}
                        {{--                        </li>--}}


{{--                        <li class="{{ (isset($active) && $active== 'category' )?'active':'' }}">--}}
{{--                            <a href="{{ route('site.category') }}">Category</a>--}}

{{--                            <ul class="dropdown">--}}


{{--                                @if (count(userRoles())> 0)--}}
{{--                                    @foreach(userRoles() AS $userRole)--}}
                                        <?php
//                                        $userRoleName = \Illuminate\Support\Str::slug($userRole, '-');
                                        ?>
{{--                                        <li class="{{ (isset($active) && $active== 'category' && $sub1_active== $userRoleName)?'active':'' }}">--}}
{{--                                            <a href="{{ route('site.category', ['type'=>$userRoleName]) }}">--}}
{{--                                                {{ $userRole }}--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    @endforeach--}}
{{--                                @endif--}}


{{--                            </ul>--}}


{{--                        </li>--}}


                        {{--                        <li class="{{ (isset($active) && $active== 'media' )?'active':'' }}">--}}
                        {{--                            <a href="javascript:void(0)">Media</a>--}}
                        {{--                            <ul class="dropdown">--}}
                        {{--                                <li class="{{ (isset($active) && $active== 'media' && $sub1_active== 'photos')?'active':'' }}">--}}
                        {{--                                    <a href="./gallery-photos.php">Photos</a>--}}
                        {{--                                </li>--}}
                        {{--                                <li class="{{ (isset($active) && $active== 'media' && $sub1_active== 'video')?'active':'' }}">--}}
                        {{--                                    <a href="javascript:void(0)">Videos</a>--}}
                        {{--                                </li>--}}
                        {{--                            </ul>--}}
                        {{--                        </li>--}}


                        {{--                        <li class="{{ (isset($active) && $active== 'contactus' )?'active':'' }}">--}}
                        {{--                            <a href="{{ route('site.contactus') }}">Contact</a>--}}
                        {{--                        </li>--}}

                    </ul>

                </nav>
            </div>
        </div>
    </div>

    <!--/ bottom nav -->

</header>
