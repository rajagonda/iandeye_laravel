<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content=""/>

    <!-- Page Title -->
    <title>@yield('title', (isset($title)?$title:'Home [I & Eye, Sharing Vision]'))</title>

    <!-- Favicon and Touch Icons -->
    <link href="/site/images/favicon.png" rel="shortcut icon" type="image/png">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
         crossorigin="anonymous">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
         crossorigin="anonymous">

    <link href="//db.onlinewebfonts.com/c/0e979bd4a3c1c6ac788ed57ac569667f?family=revicons" rel="stylesheet" type="text/css"/>



    @include('layouts.site.source')


    <style>
        .banner-text {
            background: #FFF;

        }

    </style>
    @yield('head_style')
</head>

<body class="">
<div id="wrapper" class="clearfix">
    <!-- preloader -->
    <div id="preloader">
        <div id="spinner">
            <div class="preloader-dot-loading">
                <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
            </div>
        </div>
        <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
    </div>


    @include('layouts.site.header')

    <div class="main-content">

        @yield('content')


    </div>


    @include('layouts.site.footer')
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->

<script src="/site/js/iandeye.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.1.0/jquery-migrate.min.js"></script>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


<script src="/site/js/jquery-plugin-collection.js"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="/site/js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="/site/js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

<script src="/site/js/custom.js"></script>

@yield('footer_script')


</body>

</html>
