<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title> I & EYE Admin Dashboard</title>
    <meta content="Admin Dashboard" name="description"/>
    <meta content="Mannatthemes" name="author"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="/backend/images/favicon.ico">

    <!-- jvectormap -->
    {{--    <link href="../../assets/backend/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">--}}
    {{--    <link href="../../assets/backend/plugins/fullcalendar/vanillaCalendar.css" rel="stylesheet" type="text/css"  />--}}
    {{--    <link href="../../assets/backend/plugins/morris/morris.css" rel="stylesheet">--}}

    <link href="{{ asset('/backend/css/style.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css"/>

    <style>
        .ui-widget.ui-widget-content {
            z-index: 1050 !important;
        }

        .custom-file-label {
            text-align: left;
        }

        .ui-timepicker-div .ui-widget-header {
            margin-bottom: 8px;
        }

        .ui-timepicker-div dl {
            text-align: left;
        }

        .ui-timepicker-div dl dt {
            float: left;
            clear: left;
            padding: 0 0 0 5px;
        }

        .ui-timepicker-div dl dd {
            margin: 0 10px 10px 40%;
        }

        .ui-timepicker-div td {
            font-size: 90%;
        }

        .ui-tpicker-grid-label {
            background: none;
            border: none;
            margin: 0;
            padding: 0;
        }

        .ui-timepicker-div .ui_tpicker_unit_hide {
            display: none;
        }

        .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input {
            background: none;
            color: inherit;
            border: none;
            outline: none;
            border-bottom: solid 1px #555;
            width: 95%;
        }

        .ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus {
            border-bottom-color: #aaa;
        }

        .ui-timepicker-rtl {
            direction: rtl;
        }

        .ui-timepicker-rtl dl {
            text-align: right;
            padding: 0 5px 0 0;
        }

        .ui-timepicker-rtl dl dt {
            float: right;
            clear: right;
        }

        .ui-timepicker-rtl dl dd {
            margin: 0 40% 10px 10px;
        }

        /* Shortened version style */
        .ui-timepicker-div.ui-timepicker-oneLine {
            padding-right: 2px;
        }

        .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time,
        .ui-timepicker-div.ui-timepicker-oneLine dt {
            display: none;
        }

        .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label {
            display: block;
            padding-top: 2px;
        }

        .ui-timepicker-div.ui-timepicker-oneLine dl {
            text-align: right;
        }

        .ui-timepicker-div.ui-timepicker-oneLine dl dd,
        .ui-timepicker-div.ui-timepicker-oneLine dl dd > div {
            display: inline-block;
            margin: 0;
        }

        .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
        .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before {
            content: ':';
            display: inline-block;
        }

        .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
        .ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before {
            content: '.';
            display: inline-block;
        }

        .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
        .ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before {
            display: none;
        }
    </style>

</head>


<body class="fixed-left" id="app">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner"></div>
    </div>
</div>

<!-- Begin page -->
<div id="wrapper">

@include('backend.includs.side-menu')

<!-- Start right Content here -->

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            @include('backend.includs.topbar')


            <div class="page-content-wrapper ">

            @yield('content')


            <!-- container -->

            </div> <!-- Page content Wrapper -->

        </div> <!-- content -->

        <footer class="footer">
            © 2019 © 2020 I & EYE.by Mannatthemes.
        </footer>

    </div>
    <!-- End Right content here -->

</div>
<!-- END wrapper -->


<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.1.0/jquery-migrate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
{{--<script src="{{ asset('/backend/js/admin_iandeye.js') }}" defer></script>--}}

<script src="/backend/js/modernizr.min.js"></script>
<script src="/backend/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="/backend/plugins/skycons/skycons.min.js"></script>
<script src="/backend/plugins/fullcalendar/vanillaCalendar.js"></script>
<script src="/backend/plugins/raphael/raphael-min.js"></script>
<script src="/backend/plugins/morris/morris.min.js"></script>

{{--<script src="/backend/pages/dashborad.js"></script>--}}


<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"
        crossorigin="anonymous"></script>

<!-- App js -->
<script src="/backend/js/custom.js"></script>
<script src='https://cdn.tiny.cloud/1/8s9iyl3e2ipcutwwu67ys41px1xdh6x559ji9lhat3qh5sfy/tinymce/5/tinymce.min.js'
        referrerpolicy="origin">
</script>


<script>
    tinymce.init({
        selector: '.editor',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: "30s",
        autosave_prefix: "{path}{query}-{id}-",
        autosave_restore_when_empty: false,
        autosave_retention: "2m",
        image_advtab: true,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ],
        link_list: [
            {title: 'My page 1', value: 'http://www.tinymce.com'},
            {title: 'My page 2', value: 'http://www.moxiecode.com'}
        ],
        image_list: [
            {title: 'My page 1', value: 'http://www.tinymce.com'},
            {title: 'My page 2', value: 'http://www.moxiecode.com'}
        ],
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Some class', value: 'class-name'}
        ],
        importcss_append: true,
        height: 400,
        file_picker_callback: function (callback, value, meta) {
            /* Provide file and text for the link dialog */
            if (meta.filetype === 'file') {
                callback('https://www.google.com/logos/google.jpg', {text: 'My text'});
            }

            /* Provide image and alt text for the image dialog */
            if (meta.filetype === 'image') {
                callback('https://www.google.com/logos/google.jpg', {alt: 'My alt text'});
            }

            /* Provide alternative source and posted for the media dialog */
            if (meta.filetype === 'media') {
                callback('movie.mp4', {source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg'});
            }
        },
        templates: [
            {
                title: 'New Table',
                description: 'creates a new table',
                content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>'
            },
            {title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...'},
            {
                title: 'New list with dates',
                description: 'New List with dates',
                content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>'
            }
        ],
        template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
        height: 400,
        image_caption: true,
        quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
        noneditable_noneditable_class: "mceNonEditable",
        toolbar_drawer: 'sliding',
        contextmenu: "link image imagetools table",
    });
</script>
<script type="text/javascript">
    $(function () {

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            startDate: "01-01-2015",
            endDate: "01-01-2020",
            todayBtn: "linked",
            autoclose: true,
            todayHighlight: true
        });

        $('.datetimepicker_ui').datetimepicker({
            dateFormat: "yy-mm-dd",
            timeFormat: "h:mm:ss TT",
            showOn: "focus",
            changeMonth: true,
            changeYear: true

            // startDate: "01-01-2015",
            // endDate: "01-01-2020",
            // todayBtn: "linked",
            // autoclose: true,
            // todayHighlight: true
        });
    })
</script>

@yield('footer_script')

</body>
</html>
