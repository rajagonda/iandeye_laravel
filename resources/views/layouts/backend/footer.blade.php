<script src="https://cdnjs.cloudflare.com/ajax/libs/knockout/3.4.2/knockout-min.js"></script>

<script src="{{asset('js/site.js')}}"></script>
{{-- <script src="{{asset('js/pjax.js')}}"></script> --}}

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


<script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>


<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>


<script type="text/javascript" data-turbolinks="false">

    

$(document).ready(function(){
    var documentWidth=$(document).width();

    // if (window.matchMedia('(max-width: 768px)').matches) {
    //     $('#sidebar-container').css({'position':'absolute', 'z-index':'1200'});
    // } 
    // else {
    //     $('#sidebar-container').css({'position':'fixed', 'z-index':'1200'});
    // }
        
        // if(documentWidth<600)
        // {
        //     $('#sidebar-container').css({'position':'inherit', 'z-index':'1200'});
        // }
        // else{
        //     $('#sidebar-container').css({'position':'fixed', 'z-index':'1200'});
        // }

        // if(documentWidth<600)
        // {
        //     $('#sidebar-container').css({'position':'inherit', 'z-index':'1200'});
        // }
        // else{
        //     $('#sidebar-container').css({'position':'fixed', 'z-index':'1200'});
        // }
        
});

    function SidebarCollapse() {

        console.log('click menu')

        // $('.menu-collapsed').toggleClass('d-none');
        // $('.sidebar-submenu').toggleClass('d-none');
        // $('.submenu-icon').toggleClass('d-none');
        $('#sidebar-container').toggleClass('sidebar-expanded sidebar-collapsed');

        // Treating d-flex/d-none on separators with title
        var SeparatorTitle = $('.sidebar-separator-title');
        if (SeparatorTitle.hasClass('d-flex')) {
            SeparatorTitle.removeClass('d-flex');
        } else {
            SeparatorTitle.addClass('d-flex');
        }

        // Collapse/Expand icon
        $('#collapse-icon').toggleClass('fa-chevron-circle-left fa-chevron-circle-right');
    }


    // $( "#myform" ).validate({
    //     rules: {
    //         field: {
    //             required: true,
    //             emailordomain: true
    //         }
    //     }
    // });

    $(function () {
        editor()
        $.validator.addMethod("customEmail", function (val, elem) {
            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(val)) {
                return false;
            } else {
                return true;
            }
        }, "Please enter the correct email");


        // $.each('.blur_box', function () {
        //
        // });

        $('.blur_item').removeClass('blur');
        $('.blur_loading').removeClass('show').addClass('d-none');
        // $('.blur_item').css({"-webkit-filter": "none", "filter": "none", "opacity": 1});
        // $('.blur_loading').css('display', 'none');


        $('.multiselect').multiselect({
            numberDisplayed: 0,
            // buttonContainer: ' ',
            // enableFiltering: true,
            // nSelectedText: ' Claint',
            buttonText: function (options, select) {
                if (options.length == 0) {
                    return 'Select ' + select.data('selectname');
                } else if (options.length == 1) {
                    return options.length + ' ' + select.data('selectname') + '';
                } else if (options.length > 1) {
                    return options.length + ' ' + select.data('selectname') + 's';
                }
            },
            buttonClass: 'btn btn-default text-left',
            buttonWidth: '100%',
            includeSelectAllOption: true,
            allSelectedText: 'All',
            // selectAllValue: 0,
            // selectAllNumber: false,
            maxHeight: 250,
            onSelectAll: function () {
                $('button[class="multiselect"]').attr('title', false);
            }
        });

        $('.datepicker').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: '1990:2050'
        });

        // Hide submenus
        // $('#body-row .collapse').collapse('hide');

        // $('#collapse-icon').addClass('fa-angle-double-left');
// Collapse click
        $('[data-toggle=sidebar-colapse]').click(function () {
            SidebarCollapse();
        });


        $('.select2').select2().css("width", "100%");


        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');


        });


    });

    function updateSideBarClick() {
        var documentWidth=$(document).width();

        var status = $('#sidebar_collapse').val();
        if (status == 'opened') {
            // $('#collapse-icon').addClass('fa-chevron-circle-left');
            if(documentWidth<600)
            {
                $('#sidebar_collapse').val('closed');
            }
           

        }  $.get("/toggle_sidebar", function (data, status) {
       

        });
    }

</script>
<script>
      $(document).ajaxStart(function(){
    $("#wait").css("display", "block");
  });
  $(document).ajaxComplete(function(){
    $("#wait").css("display", "none");
  });
</script>