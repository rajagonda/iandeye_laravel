<nav class="navbar navbar-expand-md navbar-dark ">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="" href="/">
        <img src="/site/images/nav-logo.png" width="120"

             class="d-inline-block align-top ml-3" alt="">
        <!-- <span class="menu-collapsed">Brand</span> -->
    </a>



    <?php $user = \Auth::user();
    $notifications = [];
    if ($user)
        $notifications = $user->userNotifications();

//     dump($notifications)
    ?>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">

        <ul class="navbar-nav ml-auto">
        <div class="float-left mr-2">
        <li class="nav-item">
        <a href="javascript:void(0);" onclick="loadNotifications()" type="button" class="nav-link m-0">
                        <i class="fa fa-bell" aria-hidden="true"></i>
                </a>

        </li>

                             <!-- The Modal -->
                                <div class="modal" id="sample">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <form class="form-horizontal sample" method="post" action="">
                                                @csrf
                                                <div class="modal-header p-2 pl-5">
                                                    <h5 class="text-muted font-sm mt-3 ml-4 mb-2">Updates over the last 24 hours</h5>
                                                    <button type="button" class="close"
                                                                data-dismiss="modal">&times;
                                                    </button>
                                                    </div>
                                                    <div class="modal-body p-0">
                                                        <div id="notificationTemplate">

                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <!-- <div class="form-group">
                                                            <div class="input-group">
                                                                <button type="submit"
                                                                        class="btn btn-warning m-0">
                                                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div> -->
                                                        <button type="button" class="btn btn-warning"
                                                                data-dismiss="modal">
                                                            <i class="fa fa-close" aria-hidden="true"></i> Close
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>

        {{--            <li class="nav-item active">--}}
        {{--                <a class="nav-link" href="#top">Home <span class="sr-only">(current)</span></a>--}}
        {{--            </li>--}}
        {{--            <li class="nav-item">--}}
        {{--                <a class="nav-link" href="#top">Features</a>--}}
        {{--            </li>--}}
        {{--            <li class="nav-item">--}}
        {{--                <a class="nav-link" href="#top">Pricing</a>--}}
        {{--            </li>--}}
        <!-- This menu is hidden in bigger devices with d-sm-none.
           The sidebar isn't proper for smaller screens imo, so this dropdown menu can keep all the useful sidebar itens exclusively for smaller screens  -->
            @if(count($notifications))
                <!-- <li class="nav-item dropleft">
                    <a class="nav-link dropdown-toggle " style="padding-right: 6px !important;"
                       href="javascript:void(0);" id="navbarDropdownMenuLink" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"><span class="badge badge-warning">{{count($notifications)}}</span></i>
                    </a>
                    <ul class="dropdown-menu dpmenu-style" aria-labelledby="navbarDropdownMenuLink">
                        <li class="border-bottom">
                            <span class="ml-2">
                                <h6 class="mb-0">Notification</h6>
                            </span>

                            <a href="{{ route('site.requirements.mark_read') }}" class="float-right mr-2">

                                <small> Mark all as read </small>
                            </a>
                        </li>

                        @foreach($notifications as $item)




                            <li class="border-bottom">
                                <a class="dropdown-item pl-2 pb-0"
                                   href="{{ route('site.requirements.mark_read', ['id'=>$item->id, 'requirements_type'=>$item->getRequirement->requirements_type]) }}">
                                    <div>
                                        {!! $item->message !!}
                                    </div>
                                    {{--  <div>
                                         <strong>
                                             {{ (isset($item->createdBy))? $item->createdBy->name: ''}}
                                         </strong>
                                     </div> --}}
                                </a>

                                <small class="pl-2">{{humainzeDateTime($item->created_at)}}</small>
                            </li>
                        @endforeach

                    </ul>
                </li> -->

            @endif
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="smallerscreenmenu" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">

                    <i class="fa fa-user" aria-hidden="true"></i> {{($user)? $user->name :''}} </a>
                <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="smallerscreenmenu">
                    <a class="dropdown-item pl-2" href="{{ route('site.users.profile',['id'=>$user->id]) }}">
                        <i class="fa fa-user-circle" aria-hidden="true"></i> Profile</a>
                    {{--    <a class="dropdown-item" href="#top">Dashboard</a>
                       <a class="dropdown-item" href="#top">Password</a> --}}
                    <a class="dropdown-item dropdown-item pl-2" onclick="logout()" href="javascript:void(0);">
                        <i class="fa fa-sign-out"></i> Logout</a>
                </div>
            </li><!-- Smaller devices menu END -->


        </ul>
    </div>
</nav><!-- NavBar END -->

<script>
    function logout() {
        $.post({
            url: '/logout',
        }).then((response) => {
            window.location.href = "<?php echo 'https://' . request()->getHttpHost(); ?>";
        }).catch((response) => {
        });
    }

    function clearNotifications() {

    }
    function loadNotifications() {
        $.ajax({
              url: "/notifications/updates",
               success: function(result){
                console.log(result);
                $('#notificationTemplate').html(result);
                $('#sample').modal('show');
               }
            }).done(function() {

        });

    }
</script>
