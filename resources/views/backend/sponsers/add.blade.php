@extends('layouts.backend')

@section('content')


    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="btn-group float-right">
                            <ol class="breadcrumb hide-phone p-0 m-0">
                                <li class="breadcrumb-item"><a href="#">Zoter</a></li>
                                <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                <li class="breadcrumb-item active">Basic</li>
                            </ol>
                        </div>
                        <h4 class="page-title">
                            Sponsers
                        </h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- end page title end breadcrumb -->
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            <h4 class="mt-0 header-title">Sponsers Add</h4>


                            {{--                            <p class="text-muted mb-4 font-14">Add <code>.table-bordered</code> for--}}
                            {{--                                borders on all sides of the table and cells.--}}
                            {{--                            </p>--}}


                            <form role="form" class="form-horizontal" method="post" enctype="multipart/form-data">
                                @csrf

                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-8">
                                        <div class="form-group row">

                                            <div class="col-sm-12 ml-auto input-group mt-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-normal">Name</span>
                                                </div>
                                                <input type="text" name="name" class="form-control" aria-label="Normal"
                                                       aria-describedby="inputGroup-sizing-sm">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12 ">
                                                <div class="input-group mt-2">
                                                    <div class="custom-file">
                                                        <input type="file" name="image" class="custom-file-input"
                                                               id="inputGroupFile04">
                                                        <label class="custom-file-label" for="inputGroupFile04">Choose
                                                            file</label>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">

                                            <div
                                                class="col-sm-12 ml-auto input-group mt-3">
                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                </div>

                                                <select class="form-control"
                                                        name="status">


                                                    <option value="">Select Status
                                                    </option>

                                                    @if (count(status_list()))
                                                        @foreach(status_list() AS $staus_key=>$staus)
                                                            <option

                                                                value="{{ $staus_key }}">
                                                                {{  $staus }}
                                                            </option>
                                                        @endforeach

                                                    @endif


                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-9 offset-sm-2">
                                                <button class="btn btn-primary">
                                                    Save
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-2"></div>
                                </div>


                            </form>


                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div><!-- container -->

    </div>


@endsection
