@extends('layouts.backend')

@section('content')



    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">

                        <h4 class="page-title">
                            Media
                        </h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- end page title end breadcrumb -->
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">


                            {{--                            <p class="text-muted mb-4 font-14">Add <code>.table-bordered</code> for--}}
                            {{--                                borders on all sides of the table and cells.--}}
                            {{--                            </p>--}}


                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mt-0 header-title">
                                        Gallery
                                    </h4>
                                </div>
                                <div class="col-md-6 text-right">

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary " data-toggle="modal"
                                            data-target="#addModel">
                                        Add
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade bd-example-modal-lg" id="addModel" tabindex="-1"
                                         role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Gallery
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <form role="form" class="form-horizontal" method="post"
                                                          enctype="multipart/form-data">
                                                        @csrf

                                                        <input type="hidden" name="action" value="add"/>

                                                        <div class="row">

                                                            <div class="col-12">

                                                                <div class="form-group row">

                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                album_name
                                                                            </div>
                                                                        </div>
                                                                        <input type="text" name="album_name" required
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">

                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                                        </div>

                                                                        <select class="form-control" required
                                                                                name="status">


                                                                            <option value="">Select Status
                                                                            </option>

                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option
                                                                                        {{--                                                                                        {{ $list->status == $staus_key ?'selected':'' }}--}}
                                                                                        value="{{ $staus_key }}">
                                                                                        {{  $staus }}
                                                                                    </option>
                                                                                @endforeach

                                                                            @endif


                                                                        </select>

                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div class="col-sm-9 offset-sm-2">
                                                                        <button class="btn btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>


                                                    </form>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    {{--                                                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    {{ form_flash_message('flash_message') }}
                                </div>
                            </div>


                            <?php
                            //                            dump($lists);
                            ?>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>album name</th>

                                    <th>Images</th>
                                    <th>status</th>

                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if (count($lists)> 0)
                                    @foreach($lists AS $list)
                                        <tr>
                                            <th scope="row">
                                                {{ $list->id }}
                                            </th>
                                            <td>
                                                {{ $list->album_name }}
                                            </td>

                                            <td>
                                                <?php
                                                //                                                dump($list->galleryImages);
                                                ?>

                                                {{ $list->galleryImages->count() }}

                                            </td>


                                            <td>

                                                {{ status_list($list->status) }}

                                            </td>


                                            <td>


                                                <!-- Button to Open the Modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#images_{{ $list->id }}">
                                                    Images
                                                </button>

                                                <!-- The Modal -->
                                                <div class="modal fade bd-example-modal-lg" id="images_{{ $list->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Images</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">


                                                                <div class="row">

                                                                    @if (count($list->galleryImages)> 0)
                                                                        @foreach($list->galleryImages AS $galleryimage)

                                                                            <div class="col-md-2">
                                                                                <img class="img-fluid"
                                                                                     src="{{ \App\Models\MediaGalleryImages::uploadDir('url').'/'.$galleryimage->image_name }}"/>



                                                                                <form method="POST" action=""
                                                                                      accept-charset="UTF-8"
                                                                                      class="form-horizontal"
                                                                                      style="display:inline">
                                                                                    {{ csrf_field() }}
                                                                                    <input type="hidden" name="id"
                                                                                           value="{{ $list->id }}"/>


                                                                                    <input type="hidden" name="action"
                                                                                           value="imageremove"/>



                                                                                    <input type="hidden" name="id"
                                                                                           value="{{ $list->id }}"/>
                                                                                    <input type="hidden" name="imageid"
                                                                                           value="{{ $galleryimage->id  }}"/>
                                                                                    <button type="submit"
                                                                                            class="btn btn-sm btn-primary"
                                                                                            title="Delete Banner"
                                                                                            onclick="return confirm('Confirm delete')">
                                                                                        Remove
                                                                                    </button>


                                                                                </form>


                                                                            </div>



                                                                        @endforeach

                                                                    @endif


                                                                </div>

                                                                <br/>
                                                                <br/>
                                                                <br/>
                                                                <br/>
                                                                <form role="form" class="form-horizontal"
                                                                      action=""
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <input type="hidden" name="action"
                                                                           value="imagesAdd"/>
                                                                    <input type="hidden" name="id"
                                                                           value="{{ $list->id }}"/>

                                                                    <div class="row">

                                                                        <div class="col-12">


                                                                            <div class="form-group row">
                                                                                <div class="col-md-12 ">
                                                                                    <div class="input-group mt-2">
                                                                                        <div class="custom-file">
                                                                                            <input type="file" multiple
                                                                                                   name="photo[]"
                                                                                                   class="custom-file-input"
                                                                                                   id="inputGroupFile04">
                                                                                            <label
                                                                                                class="custom-file-label"
                                                                                                for="inputGroupFile04">
                                                                                                Choose file
                                                                                            </label>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>


                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div class="col-sm-9 offset-sm-2">
                                                                                    <button class="btn btn-primary">
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>


                                                                </form>


                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Button to Open the Modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#Edit_{{ $list->id }}">
                                                    Edit
                                                </button>

                                                <!-- The Modal -->
                                                <div class="modal fade bd-example-modal-lg" id="Edit_{{ $list->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Modal Heading</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">

                                                                <form role="form" class="form-horizontal"
                                                                      action=""
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <input type="hidden" name="action" value="edit"/>
                                                                    <input type="hidden" name="id"
                                                                           value="{{ $list->id }}"/>

                                                                    <div class="row">

                                                                        <div class="col-12">


                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            album name
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="text" name="album_name"
                                                                                           required
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->album_name }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                                                    </div>

                                                                                    <select class="form-control"
                                                                                            required
                                                                                            name="status">


                                                                                        <option value="">Select Status
                                                                                        </option>

                                                                                        @if (count(status_list()))
                                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                                <option
                                                                                                    {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                    value="{{ $staus_key }}">
                                                                                                    {{  $staus }}
                                                                                                </option>
                                                                                            @endforeach

                                                                                        @endif


                                                                                    </select>

                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div class="col-sm-9 offset-sm-2">
                                                                                    <button class="btn btn-primary">
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>


                                                                </form>


                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                                {{--                                                <form method="POST" action="" accept-charset="UTF-8"--}}
                                                {{--                                                      class="form-horizontal" style="display:inline">--}}
                                                {{--                                                    {{ csrf_field() }}--}}
                                                {{--                                                    <input type="hidden" name="id" value="{{ $list->id }}"/>--}}
                                                {{--                                                    <input type="hidden" name="action" value="remove"/>--}}
                                                {{--                                                    <button type="submit" class="btn btn-sm btn-primary"--}}
                                                {{--                                                            title="Delete Banner"--}}
                                                {{--                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">--}}
                                                {{--                                                        Remove--}}
                                                {{--                                                    </button>--}}

                                                {{--                                                </form>--}}


                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div><!-- container -->

    </div>


@endsection
@section('footer_script')

@endsection
