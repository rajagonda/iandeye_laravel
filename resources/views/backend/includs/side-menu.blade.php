<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <!--<a href="index.html" class="logo"><i class="mdi mdi-assistant"></i>Zoter</a>-->
            <a href="{{ route('route_admin.dashboard') }}" class="logo">
                <img src="/site/images/logo-wide.png " alt="" class="">
                {{--                <img src="/backend/images/logo-lg.png " alt="" class="logo-large">--}}
            </a>
        </div>
    </div>

    <div class="sidebar-inner niceScrollleft">

        <div id="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>

                <li>
                    <a href="{{ route('route_admin.dashboard') }}" class="waves-effect">
                        <i class="mdi mdi-airplay"></i>
                        <span> Dashboard <span class="badge badge-pill badge-primary float-right">7</span></span>
                    </a>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-layers"></i>
                        <span> Manage Data </span>
                        <span class="float-right">
                            <i class="mdi mdi-chevron-right"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ route('route_admin.sponsers') }}">

                                <span> sponsers </span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('route_admin.locations') }}">

                                <span>Locations </span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('route_admin.users') }}">

                                <span>users </span>
                            </a>
                        </li>


                        <li>
                            <a href="{{ route('route_admin.banners') }}">

                                <span>Banners </span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="{{ route('route_admin.events') }}" class="waves-effect">
                        <i class="mdi mdi-airplay"></i>
                        <span>Events </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('route_admin.projects') }}" class="waves-effect">
                        <i class="mdi mdi-airplay"></i>
                        <span>Projects </span>
                    </a>
                </li>


                <li>
                    <a href="{{ route('route_admin.successStories') }}" class="waves-effect">
                        <i class="mdi mdi-airplay"></i>
                        <span>Success Stories </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('route_admin.testimonials') }}" class="waves-effect">
                        <i class="mdi mdi-airplay"></i>
                        <span>Testimonials </span>
                    </a>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-layers"></i>
                        <span> Financials </span>
                        <span class="float-right">
                            <i class="mdi mdi-chevron-right"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ route('route_admin.activity') }}" class="waves-effect">
                                <i class="mdi mdi-airplay"></i>
                                <span>Financials Activity</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('route_admin.activity_yearwise') }}">
                                <span> Years wise Financials </span>
                            </a>
                        </li>


                    </ul>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-layers"></i>
                        <span> Media </span>
                        <span class="float-right">
                            <i class="mdi mdi-chevron-right"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ route('route_admin.media.gallery') }}">
                                <span> Gallery </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('route_admin.media.vidoes') }}">
                                <span> Video </span>
                            </a>
                        </li>


                        <li>
                            <a href="{{ route('route_admin.media.press') }}">
                                <span> Press </span>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-layers"></i>
                        <span> Submit Data </span>
                        <span class="float-right">
                            <i class="mdi mdi-chevron-right"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">


                        <li>
                            <a href="{{ route('route_admin.settings') }}">
                                <span> Settings</span>
                            </a>
                        </li>


                        <li>
                            <a href="{{ route('route_admin.contactUs') }}">

                                <span>Contact Us </span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('route_admin.joinAsVolunteer') }}">

                                <span>Register Volunteer </span>
                            </a>
                        </li>

                    </ul>
                </li>


                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-layers"></i>
                        <span> Pages </span>
                        <span class="float-right">
                            <i class="mdi mdi-chevron-right"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="{{ route('route_admin.pages.type') }}">Pages Type</a>
                        </li>
                        <li>
                            <a href="{{ route('route_admin.pages.list') }}">pages</a>
                        </li>

                    </ul>
                </li>


            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
<!-- Left Sidebar End -->
