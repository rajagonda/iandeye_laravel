@extends('layouts.backend')

@section('content')


    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">

                        <h4 class="page-title">
                            Finacial Activity
                        </h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- end page title end breadcrumb -->
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">


                            {{--                            <p class="text-muted mb-4 font-14">Add <code>.table-bordered</code> for--}}
                            {{--                                borders on all sides of the table and cells.--}}
                            {{--                            </p>--}}


                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mt-0 header-title">
                                        Finacial Activity
                                    </h4>
                                </div>
                                <div class="col-md-6 text-right">

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary " data-toggle="modal"
                                            data-target="#addModel">
                                        Add
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade bd-example-modal-lg" id="addModel" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Finacial Activity

                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <form role="form" class="form-horizontal" method="post"
                                                          enctype="multipart/form-data">
                                                        @csrf

                                                        <input type="hidden" name="action" value="add"/>

                                                        <div class="row">
                                                            <div class="col-2"></div>
                                                            <div class="col-8">


                                                                <div class="form-group row">
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        Activity Name
                                                                                    </span>
                                                                        </div>
                                                                        <input type="text" name="activity_name"
                                                                               class="form-control"
                                                                               aria-label="Normal"
                                                                               value=""
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        period
                                                                                    </span>
                                                                        </div>
                                                                        <input type="text" name="period"
                                                                               class="form-control"
                                                                               aria-label="Normal"
                                                                               value=""
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        no of beneficiaries
                                                                                    </span>
                                                                        </div>
                                                                        <input type="text" name="no_of_beneficiaries"
                                                                               class="form-control"
                                                                               aria-label="Normal"
                                                                               value=""
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                       project_cost
                                                                                    </span>
                                                                        </div>
                                                                        <input type="text" name="project_cost"
                                                                               class="form-control"
                                                                               aria-label="Normal"
                                                                               value=""
                                                                               aria-describedby="inputGroup-sizing-sm">

                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                       INR
                                                                                    </span>
                                                                        </div>
                                                                    </div>


                                                                </div>


                                                                <div class="form-group row">
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                      year
                                                                                    </span>
                                                                        </div>
                                                                        <input type="text" name="year"
                                                                               class="form-control"
                                                                               aria-label="Normal"
                                                                               value=""
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>



                                                                <div class="form-group row">

                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <textarea name="description"
                                                                                  class="form-control editor"></textarea>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">

                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                                        </div>

                                                                        <select class="form-control"
                                                                                name="status">


                                                                            <option value="">Select Status
                                                                            </option>

                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option

                                                                                        value="{{ $staus_key }}">
                                                                                        {{  $staus }}
                                                                                    </option>
                                                                                @endforeach

                                                                            @endif


                                                                        </select>

                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-9 offset-sm-2">
                                                                        <button class="btn btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-2"></div>
                                                        </div>


                                                    </form>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    {{--                                                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    {{ form_flash_message('flash_message') }}
                                </div>
                            </div>


                            <?php
                            //                            dump($lists);
                            ?>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Write-up</th>
                                    <th>period</th>
                                    <th>No Of Beneficiaries</th>
                                    <th>Project Cost</th>
                                    <th>Year</th>
                                    <th>Status</th>
                                    <th>Added Date</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if (count($lists)> 0)
                                    @foreach($lists AS $list)
                                        <tr>
                                            <th scope="row">
                                                {{ $list->id }}
                                            </th>
                                            <td>
                                                {{ $list->activity_name }}
                                            </td>

                                            <td>
                                                {{ $list->description }}
                                            </td>

                                            <td>
                                                {{ $list->period }}
                                            </td>
                                            <td>
                                                {{ $list->no_of_beneficiaries }}
                                            </td>
                                            <td>
                                                {{ $list->project_cost }}
                                            </td>
                                            <td>
                                                {{ $list->year }}
                                            </td>


                                            <td>

                                                {{ status_list($list->status) }}
                                            </td>
                                            <td>
                                                {{ $list->created_at }}
                                            </td>
                                            <td>

                                                <!-- Button to Open the Modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#Edit_{{ $list->id }}">
                                                    Edit
                                                </button>

                                                <!-- The Modal -->
                                                <div class="modal fade bd-example-modal-lg" id="Edit_{{ $list->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Modal Heading</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">

                                                                <form role="form" class="form-horizontal"
                                                                      action=""
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <input type="hidden" name="action" value="edit"/>
                                                                    <input type="hidden" name="id"
                                                                           value="{{ $list->id }}"/>

                                                                    <div class="row">
                                                                        <div class="col-2"></div>
                                                                        <div class="col-8">


                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        Activity Name
                                                                                    </span>
                                                                                    </div>
                                                                                    <input type="text" name="activity_name"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->activity_name }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        period
                                                                                    </span>
                                                                                    </div>
                                                                                    <input type="text" name="period"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->period }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        no of beneficiaries
                                                                                    </span>
                                                                                    </div>
                                                                                    <input type="text" name="no_of_beneficiaries"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->no_of_beneficiaries }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                       project_cost
                                                                                    </span>
                                                                                    </div>
                                                                                    <input type="text" name="project_cost"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->project_cost }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">

                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                       INR
                                                                                    </span>
                                                                                    </div>
                                                                                </div>


                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                      year
                                                                                    </span>
                                                                                    </div>
                                                                                    <input type="text" name="year"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->year }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>






                                                                            <div class="form-group row">
                                                                                <?php
                                                                                //                                                                                dump($list);
                                                                                ?>

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <textarea name="description"
                                                                                              class="form-control editor">{{ $list->description }}</textarea>
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                                                    </div>

                                                                                    <select class="form-control"
                                                                                            name="status">


                                                                                        <option value="">Select Status
                                                                                        </option>

                                                                                        @if (count(status_list()))
                                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                                <option
                                                                                                    {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                    value="{{ $staus_key }}">
                                                                                                    {{  $staus }}
                                                                                                </option>
                                                                                            @endforeach

                                                                                        @endif


                                                                                    </select>

                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div class="col-sm-9 offset-sm-2">
                                                                                    <button class="btn btn-primary">
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-2"></div>
                                                                    </div>


                                                                </form>


                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                                <form method="POST" action="" accept-charset="UTF-8"
                                                      class="form-horizontal" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $list->id }}"/>
                                                    <input type="hidden" name="action" value="remove"/>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            title="Delete Banner"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        Remove
                                                    </button>

                                                </form>


                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div><!-- container -->

    </div>


@endsection
