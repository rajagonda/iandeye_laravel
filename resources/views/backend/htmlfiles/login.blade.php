<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <title>I & EYE Admin Dashboard</title>
        <meta content="Admin Dashboard" name="description" />
        <meta content="Mannatthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <link rel="shortcut icon" href="/backend/images/favicon.ico">

        <link href="../../../assets/backend/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="../../../assets/backend/css/icons.css" rel="stylesheet" type="text/css">
        <link href="../../../assets/backend/css/style.css" rel="stylesheet" type="text/css">

    </head>
    <body>


    <!-- Begin page -->
    <div class="accountbg"></div>
    <div class="wrapper-page">

        <div class="card">
            <div class="card-body">

                <div class="text-center">
                    <a href="index.html" class="logo logo-admin"><img src="/backend/images/e-logo.png" height="20" alt="logo"></a>
                </div>

                <div class="px-3 pb-3">
                    <form class="form-horizontal m-t-20" action="index.html">

                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control" type="text" required="" placeholder="Username">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control" type="password" required="" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group text-center row m-t-20">
                            <div class="col-12">
                                <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-10 mb-0 row">
                            <div class="col-sm-7 m-t-20">
                                <a href="pages-recoverpw.html" class="text-muted"><i class="mdi mdi-lock"></i> <small>Forgot your password ?</small></a>
                            </div>
                            <div class="col-sm-5 m-t-20">
                                <a href="pages-register.html" class="text-muted"><i class="mdi mdi-account-circle"></i> <small>Create an account ?</small></a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



        <!-- jQuery  -->
        <script src="../../../assets/backend/js/jquery.min.js"></script>
        <script src="../../../assets/backend/js/popper.min.js"></script>
        <script src="../../../assets/backend/js/bootstrap.min.js"></script>
        <script src="../../../assets/backend/js/modernizr.min.js"></script>
        <script src="../../../assets/backend/js/detect.js"></script>
        <script src="../../../assets/backend/js/fastclick.js"></script>
        <script src="../../../assets/backend/js/jquery.blockUI.js"></script>
        <script src="../../../assets/backend/js/waves.js"></script>
        <script src="../../../assets/backend/js/jquery.nicescroll.js"></script>

        <!-- App js -->
        <script src="../../assets/backend/js/app.js"></script>

    </body>
</html>
