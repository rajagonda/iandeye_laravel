@extends('layouts.backend')

@section('content')


    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">

                        <h4 class="page-title">
                            Locations
                        </h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- end page title end breadcrumb -->
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">


                            {{--                            <p class="text-muted mb-4 font-14">Add <code>.table-bordered</code> for--}}
                            {{--                                borders on all sides of the table and cells.--}}
                            {{--                            </p>--}}


                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mt-0 header-title">
                                        Locations
                                    </h4>
                                </div>
                                <div class="col-md-6 text-right">

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary " data-toggle="modal"
                                            data-target="#addModel">
                                        Add
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade bd-example-modal-lg" id="addModel" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Locations

                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <form role="form" class="form-horizontal" method="post"
                                                          enctype="multipart/form-data">
                                                        @csrf

                                                        <input type="hidden" name="action" value="add"/>

                                                        <div class="row">
                                                            <div class="col-2"></div>
                                                            <div class="col-8">
                                                                <div class="form-group row">

                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="inputGroup-sizing-normal">Name</span>
                                                                        </div>
                                                                        <input type="text" name="name"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">

                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <textarea name="description"
                                                                                  class="form-control editor"></textarea>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div class="col-md-12 ">
                                                                        <div class="input-group mt-2">
                                                                            <div class="custom-file">
                                                                                <input type="file"
                                                                                       name="photo"
                                                                                       class="custom-file-input"
                                                                                       id="inputGroupFile04">
                                                                                <label
                                                                                    class="custom-file-label"
                                                                                    for="inputGroupFile04">
                                                                                    Choose file
                                                                                </label>
                                                                            </div>

                                                                        </div>
                                                                    </div>

                                                                    {{--                                                                    <a data-fancybox="gallery" href="{{ \App\Models\Page::uploadDir('url').'/'.$list->photo }}">--}}
                                                                    {{--                                                                        <img width="50"--}}
                                                                    {{--                                                                             src="{{ \App\Models\Page::uploadDir('url').'/'.$list->photo }}"/>--}}
                                                                    {{--                                                                    </a>--}}

                                                                </div>


                                                                <div class="form-group row">
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        Projects
                                                                                    </span>
                                                                        </div>

                                                                        <select class="form-control"
                                                                                name="project_id">
                                                                            <option value="">
                                                                                Project
                                                                            </option>
                                                                            @if (count($projects))
                                                                                @foreach($projects AS $project)
                                                                                    <option
                                                                                        value="{{ $project->id }}">
                                                                                        {{  $project->name }}
                                                                                    </option>
                                                                                @endforeach
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">

                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                                        </div>

                                                                        <select class="form-control"
                                                                                name="status">


                                                                            <option value="">Select Status
                                                                            </option>

                                                                            @if (count(status_list()))
                                                                                @foreach(status_list() AS $staus_key=>$staus)
                                                                                    <option

                                                                                        value="{{ $staus_key }}">
                                                                                        {{  $staus }}
                                                                                    </option>
                                                                                @endforeach

                                                                            @endif


                                                                        </select>

                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-9 offset-sm-2">
                                                                        <button class="btn btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-2"></div>
                                                        </div>


                                                    </form>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    {{--                                                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    {{ form_flash_message('flash_message') }}
                                </div>
                            </div>


                            <?php
                            //                            dump($lists);
                            ?>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Adress</th>
                                    <th>Photo</th>
                                    <th>Projct</th>

                                    <th>Status</th>
                                    <th>Added Date</th>
                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if (count($lists)> 0)
                                    @foreach($lists AS $list)
                                        <tr>
                                            <th scope="row">
                                                {{ $list->id }}
                                            </th>
                                            <td>
                                                {{ $list->name }}
                                            </td>
                                            <td>
                                                {{ $list->description }}
                                            </td>

                                            <td>
                                                @if ($list->photo != '')

                                                    <a data-fancybox="gallery"
                                                       href="{{ \App\Models\Location::uploadDir('url').'/'.$list->photo }}">
                                                        <img width="50"
                                                             src="{{ \App\Models\Location::uploadDir('url').'/'.$list->photo }}"/>
                                                    </a>

                                                @endif
                                            </td>
                                            <td>
                                                <?php
//                                                                                                dump($list->projects->pluck('name','id')->toArray());
                                                ?>


                                                {{ implode(',', $list->projects->pluck('name','id')->toArray()) }}

{{--                                                {{ (isset($list->projects)) ? $list->projects->name : '' }}--}}
                                            </td>

                                            <td>
                                                {{--                                                {{ $list->status }}--}}


                                                {{ status_list($list->status) }}
                                            </td>
                                            <td>
                                                {{ $list->created_at }}
                                            </td>
                                            <td>

                                                <!-- Button to Open the Modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#Edit_{{ $list->id }}">
                                                    Edit
                                                </button>

                                                <!-- The Modal -->
                                                <div class="modal fade bd-example-modal-lg" id="Edit_{{ $list->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Modal Heading</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">

                                                                <form role="form" class="form-horizontal"
                                                                      action=""
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <input type="hidden" name="action" value="edit"/>
                                                                    <input type="hidden" name="id"
                                                                           value="{{ $list->id }}"/>

                                                                    <div class="row">
                                                                        <div class="col-2"></div>
                                                                        <div class="col-8">
                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Name</span>
                                                                                    </div>
                                                                                    <input type="text" name="name"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->name }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                        <textarea name="description"
                                                                                  class="form-control editor">{{ $list->description }}</textarea>
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div class="col-md-12 ">
                                                                                    <div class="input-group mt-2">
                                                                                        <div class="custom-file">
                                                                                            <input type="file"
                                                                                                   name="photo"
                                                                                                   class="custom-file-input"
                                                                                                   id="inputGroupFile04">
                                                                                            <label
                                                                                                class="custom-file-label"
                                                                                                for="inputGroupFile04">
                                                                                                Choose file
                                                                                            </label>
                                                                                        </div>

                                                                                    </div>
                                                                                </div>

                                                                                @if ($list->photo != '')

                                                                                    <a data-fancybox="gallery"
                                                                                       href="{{ \App\Models\Location::uploadDir('url').'/'.$list->photo }}">
                                                                                        <img width="50"
                                                                                             src="{{ \App\Models\Location::uploadDir('url').'/'.$list->photo }}"/>
                                                                                    </a>

                                                                                @endif


                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">
                                                                                        Projects
                                                                                    </span>
                                                                                    </div>

                                                                                    <select class="form-control"
                                                                                            name="project_id">
                                                                                        <option value="">
                                                                                            Project
                                                                                        </option>
                                                                                        @if (count($projects))
                                                                                            @foreach($projects AS $project)
                                                                                                <option   {{ $project->id == $list->project_id ?'selected':'' }}
                                                                                                    value="{{ $project->id }}">
                                                                                                    {{  $project->name }}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </select>
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                    <span class="input-group-text"
                                                                                          id="inputGroup-sizing-normal">Status</span>
                                                                                    </div>

                                                                                    <select class="form-control"
                                                                                            name="status">


                                                                                        <option value="">Select Status
                                                                                        </option>

                                                                                        @if (count(status_list()))
                                                                                            @foreach(status_list() AS $staus_key=>$staus)
                                                                                                <option
                                                                                                    {{ $list->status == $staus_key ?'selected':'' }}
                                                                                                    value="{{ $staus_key }}">
                                                                                                    {{  $staus }}
                                                                                                </option>
                                                                                            @endforeach

                                                                                        @endif


                                                                                    </select>

                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div class="col-sm-9 offset-sm-2">
                                                                                    <button class="btn btn-primary">
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="col-2"></div>
                                                                    </div>


                                                                </form>


                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


                                                <form method="POST" action="" accept-charset="UTF-8"
                                                      class="form-horizontal" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $list->id }}"/>
                                                    <input type="hidden" name="action" value="remove"/>
                                                    <button type="submit" class="btn btn-sm btn-primary"
                                                            title="Delete Banner"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">
                                                        Remove
                                                    </button>

                                                </form>


                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div><!-- container -->

    </div>


@endsection
