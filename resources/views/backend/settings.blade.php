@extends('layouts.backend')

@section('content')



    <div class="page-content-wrapper ">

        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">

                        <h4 class="page-title">
                            Settings
                        </h4>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <!-- end page title end breadcrumb -->
            <div class="row">


                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">


                            {{--                            <p class="text-muted mb-4 font-14">Add <code>.table-bordered</code> for--}}
                            {{--                                borders on all sides of the table and cells.--}}
                            {{--                            </p>--}}


                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mt-0 header-title">
                                        Settings
                                    </h4>
                                </div>
                                <div class="col-md-6 text-right">

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary " data-toggle="modal"
                                            data-target="#addModel">
                                        Add
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade bd-example-modal-lg" id="addModel" tabindex="-1"
                                         role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Settings
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <form role="form" class="form-horizontal" method="post"
                                                          enctype="multipart/form-data">
                                                        @csrf

                                                        <input type="hidden" name="action" value="add"/>

                                                        <div class="row">

                                                            <div class="col-12">


                                                                <div class="form-group row">
                                                                    <label>
                                                                        Home About
                                                                    </label>
                                                                    <div
                                                                        class="col-sm-12 ml-auto input-group mt-3">
                                                                        <textarea name="home_about"
                                                                                  class="form-control editor"></textarea>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">

                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                conatct address
                                                                            </div>
                                                                        </div>
                                                                        <input type="text" name="conatct_address"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">

                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                conatct email
                                                                            </div>
                                                                        </div>
                                                                        <input type="text" name="conatct_email"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                conatct phone
                                                                            </div>
                                                                        </div>
                                                                        <input type="text" name="conatct_phone"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                Facebook Url
                                                                            </div>
                                                                        </div>
                                                                        <input type="url" name="social_fb"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                Twitter Url
                                                                            </div>
                                                                        </div>
                                                                        <input type="url" name="social_tw"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                Linked In Url
                                                                            </div>
                                                                        </div>
                                                                        <input type="url" name="social_in"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <div class="col-sm-12 ml-auto input-group mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text"
                                                                                 id="inputGroup-sizing-normal">
                                                                                Instagram Url
                                                                            </div>
                                                                        </div>
                                                                        <input type="url" name="social_insta"
                                                                               class="form-control" aria-label="Normal"
                                                                               aria-describedby="inputGroup-sizing-sm">
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <div class="col-sm-9 offset-sm-2">
                                                                        <button class="btn btn-primary">
                                                                            Save
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>


                                                    </form>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">Close
                                                    </button>
                                                    {{--                                                    <button type="button" class="btn btn-primary">Save changes</button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    {{ form_flash_message('flash_message') }}
                                </div>
                            </div>


                            <?php
                            //                            dump($lists);
                            ?>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>conatct_phone</th>
                                    <th>conatct_email</th>
                                    <th>conatct_address</th>
                                    <th>links</th>

                                    <th>action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @if (count($lists)> 0)
                                    @foreach($lists AS $list)
                                        <tr>
                                            <th scope="row">
                                                {{ $list->id }}
                                            </th>
                                            <td>
                                                {{ $list->conatct_phone }}
                                            </td>


                                            <td>

                                                {{ $list->conatct_email }}
                                            </td>
                                            <td>

                                                {!! $list->conatct_address !!}
                                            </td>
                                            <td>

                                                {{ $list->social_fb }}
                                                {{ $list->social_tw }}
                                                {{ $list->social_in }}
                                                {{ $list->social_insta }}
                                            </td>


                                            <td>

                                                <!-- Button to Open the Modal -->
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#Edit_{{ $list->id }}">
                                                    Edit
                                                </button>

                                                <!-- The Modal -->
                                                <div class="modal fade bd-example-modal-lg" id="Edit_{{ $list->id }}">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">

                                                            <!-- Modal Header -->
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Modal Heading</h4>
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal">&times;
                                                                </button>
                                                            </div>

                                                            <!-- Modal body -->
                                                            <div class="modal-body">

                                                                <form role="form" class="form-horizontal"
                                                                      action=""
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    @csrf

                                                                    <input type="hidden" name="action" value="edit"/>
                                                                    <input type="hidden" name="id"
                                                                           value="{{ $list->id }}"/>

                                                                    <div class="row">

                                                                        <div class="col-12">


                                                                            <div class="form-group row">
                                                                                <label>
                                                                                    Home About
                                                                                </label>
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <textarea name="home_about"
                                                                                              class="form-control editor">{{ $list->home_about }}</textarea>
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            conatct address
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="text"
                                                                                           name="conatct_address"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->conatct_address }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">

                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            conatct email
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="text"
                                                                                           name="conatct_email"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->conatct_email }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            conatct phone
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="text"
                                                                                           name="conatct_phone"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->conatct_phone }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            Facebook Url
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="url" name="social_fb"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->social_fb }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            Twitter Url
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="url" name="social_tw"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->social_tw }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            Linked In Url
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="url" name="social_in"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           value="{{ $list->social_in }}"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group row">
                                                                                <div
                                                                                    class="col-sm-12 ml-auto input-group mt-3">
                                                                                    <div class="input-group-prepend">
                                                                                        <div class="input-group-text"
                                                                                             id="inputGroup-sizing-normal">
                                                                                            Instagram Url
                                                                                        </div>
                                                                                    </div>
                                                                                    <input type="url"
                                                                                           name="social_insta"
                                                                                           value="{{ $list->social_insta }}"
                                                                                           class="form-control"
                                                                                           aria-label="Normal"
                                                                                           aria-describedby="inputGroup-sizing-sm">
                                                                                </div>
                                                                            </div>





                                                                            <div class="form-group row">
                                                                                <div class="col-sm-9 offset-sm-2">
                                                                                    <button class="btn btn-primary">
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </div>


                                                                </form>


                                                            </div>

                                                            <!-- Modal footer -->
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>


{{--                                                <form method="POST" action="" accept-charset="UTF-8"--}}
{{--                                                      class="form-horizontal" style="display:inline">--}}
{{--                                                    {{ csrf_field() }}--}}
{{--                                                    <input type="hidden" name="id" value="{{ $list->id }}"/>--}}
{{--                                                    <input type="hidden" name="action" value="remove"/>--}}
{{--                                                    <button type="submit" class="btn btn-sm btn-primary"--}}
{{--                                                            title="Delete Banner"--}}
{{--                                                            onclick="return confirm(&quot;Confirm delete?&quot;)">--}}
{{--                                                        Remove--}}
{{--                                                    </button>--}}

{{--                                                </form>--}}


                                            </td>
                                        </tr>
                                    @endforeach
                                @endif


                                </tbody>
                            </table>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div><!-- container -->

    </div>


@endsection
@section('footer_script')

@endsection
