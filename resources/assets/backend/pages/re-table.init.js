
/*
Template Name: I & EYE Admin Dashboard
 Author: Mannatthemes
 Website: www.mannatthemes.com
 File: Re-Table init js
 */

$(function() {
    $('.table-responsive').responsiveTable({
        addDisplayAllBtn: 'btn btn-secondary'
    });
});
