require('../../../js/app.js');

require('./jquery.min');
require('./bootstrap.min');
require('./modernizr.min');
require('./detect');
require('./fastclick');
require('./jquery.blockUI');
// require('./waves');
require('./jquery.nicescroll');
