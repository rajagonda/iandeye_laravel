const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
//
// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');


mix
    .js('resources/assets/site/js/iandeye.js', 'public/site/js')
    .copy('resources/assets/site/js/jquery-plugin-collection.js', 'public/site/js')
    .js('resources/assets/site/js/custom.js', 'public/site/js')
    .sass('resources/assets/site/css/iandeye.scss', 'public/site/css');

mix.copyDirectory('resources/assets/site/js/revolution-slider', 'public/site/js/revolution-slider');
mix.copyDirectory('resources/assets/site/images', 'public/site/images');

mix.copyDirectory('resources/assets/backend/images', 'public/backend/images');
mix.copyDirectory('resources/assets/backend/pages', 'public/backend/pages');
mix.copyDirectory('resources/assets/backend/plugins', 'public/backend/plugins');

mix
    .js('resources/assets/backend/js/admin_iandeye.js', 'public/backend/js')
    .copy('resources/assets/backend/js/modernizr.min.js', 'public/backend/js')
    .copy('resources/assets/backend/js/custom.js', 'public/backend/js')
    .sass('resources/assets/backend/scss/style.scss', 'public/backend/css');
