<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Conatctus;
use App\Models\Event;
use App\Models\FinancialsActivity;
use App\Models\FinancialYear;
use App\Models\Location;
use App\Models\MediaGallery;
use App\Models\MediaGalleryImage;
use App\Models\MediaGalleryImages;
use App\Models\MediaPress;
use App\Models\MediaVideo;
use App\Models\Page;
use App\Models\PageType;
use App\Models\Project;
use App\Models\RegisterValunteer;
use App\Models\Setting;
use App\Models\Sponsor;
use App\Models\SuccessStories;
use App\Models\Testimonials;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = array();
        $data['title'] = 'dashboard';
        $data['active'] = 'dashboard';
        $data['sub_active'] = 'dashboard';
        $data['sub1_active'] = 'dashboard';
        return view('backend.dashbords', $data);
    }

    public function pagesTypes(Request $request)
    {
        $data = array();
        $data['title'] = 'pages types';
        $data['active'] = 'pages';
        $data['sub_active'] = 'types';
        $data['sub1_active'] = '';
        $data['lists'] = PageType::get();
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    $insertData['alias'] = Str::slug($insertData['name'], '-');;
//                    $insertData['image'] = Sponsor::uploadImage($request, 'photo');
//                    dd($insertData);
                    PageType::create($insertData);
                    return redirect()->route('route_admin.pages.type')->with('flash_message', 'Pages type Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = PageType::where('id', $request->id)->first();
                    $updateData['alias'] = Str::slug($updateData['name'], '-');;
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('route_admin.pages.type')->with('flash_message', 'Pages types updated !');
                    break;
                case 'remove':
                    $updateData = $request->all();
                    $item = PageType::where('id', $request->id)->first();
                    if (!empty($item)) {
                        PageType::destroy($request->id);
                    }
                    return redirect()->route('route_admin.pages.type')->with('flash_message', 'Pages types Removed !');
                    break;
            }
//            dd($insertData);
        }
        return view('backend.pages.page_types', $data);
    }

    public function pagesList(Request $request)
    {
        $data = array();
        $data['title'] = 'pages types';
        $data['active'] = 'pages';
        $data['sub_active'] = 'types';
        $data['sub1_active'] = '';
        $data['lists'] = Page::with('pageTypes')->get();
        $data['page_types'] = PageType::where('status', 1)->get();
        if ($request->isMethod('post')) {
            switch ($request->action) {
                case 'add':
                    $insertData = $request->all();
                    $insertData['alias'] = Str::slug($insertData['title'], '-');;
                    $insertData['photo'] = Page::uploadImage($request, 'photo');
//                    dd($insertData);
                    Page::create($insertData);
                    return redirect()->route('route_admin.pages.list')->with('flash_message', 'Pages type Added!');
                    break;
                case 'edit':
                    $updateData = $request->all();
                    $item = Page::where('id', $request->id)->first();
                    $updateData['alias'] = Str::slug($updateData['title'], '-');
                    if ($request->hasFile('photo')) {
                        $updateData['photo'] = Page::uploadImage($request, 'photo', $item->photo);
                    }
                    if (!empty($item)) {
                        $item->update($updateData);
                    }
                    return redirect()->route('route_admin.pages.list')->with('flash_message', 'Pages types updated !');
                    break;
                case 'remove':
                    $updateData = $request->all();
                    $item = Page::where('id', $request->id)->first();
                    if (!empty($item)) {
                        Page::destroy($request->id);
                    }
                    return redirect()->route('route_admin.pages.list')->with('flash_message', 'Pages types Removed !');
                    break;
            }
//            dd($insertData);
        }
        return view('backend.pages.pages', $data);
    }

    public function sponsers(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'sponsers';
        $data['active'] = 'sponsers';
        $data['sub_active'] = 'sponsers';
        $data['sub1_active'] = 'sponsers';
        if ($action != null) {
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
//                        dump($request->all());
                        $insertData = $request->all();
                        $insertData['alias'] = Str::slug($insertData['name'], '-');;
                        $insertData['image'] = Sponsor::uploadImage($request, 'image');
//                        dd($insertData);
                        Sponsor::create($insertData);
                        return redirect()->route('route_admin.sponsers')->with('flash_message', 'sponsers Added!');
                    }
                    return view('backend.sponsers.add', $data);
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Sponsor::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $updateData['alias'] = Str::slug($updateData['name'], '-');
                            if ($request->hasFile('image')) {
                                $updateData['image'] = Sponsor::uploadImage($request, 'image', $getItem->image);
                            }
                        }
                        $getItem->update($updateData);
                        return redirect()->route('route_admin.sponsers')->with('flash_message', 'sponsers updated!');
                    }
                    dd('edit');
                    break;
                case 'remove':
                    $getItem = Sponsor::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        if ($getItem->image != '') {
                            $filePath = Sponsor::uploadDir();
                            File::delete($filePath . '/' . $getItem->image);
                        }
                        Sponsor::destroy($request->id);
                    }
                    return redirect()->route('route_admin.sponsers')->with('flash_message', 'sponsers updated!');
                    break;
            }
        } else {
            $data['lists'] = Sponsor::get();
            return view('backend.sponsers.lists', $data);
        }
    }

    public function locations(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'sponsers';
        $data['active'] = 'sponsers';
        $data['sub_active'] = 'sponsers';
        $data['sub1_active'] = 'sponsers';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();
                        $insertData['alias'] = Str::slug($insertData['name'], '-');
                        if ($request->hasFile('photo')) {
                            $insertData['photo'] = Location::uploadImage($request, 'photo');
                        }
//                        dd($insertData);
                        Location::create($insertData);
                        return redirect()->route('route_admin.locations')->with('flash_message', 'Location Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Location::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $updateData['alias'] = Str::slug($updateData['name'], '-');
                            if ($request->hasFile('photo')) {
                                $updateData['photo'] = Location::uploadImage($request, 'photo', $getItem->photo);
                            }
                        }
                        $getItem->update($updateData);
                        return redirect()->route('route_admin.locations')->with('flash_message', 'Location updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Location::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        if ($getItem->image != '') {
                            $filePath = Location::uploadDir();
                            File::delete($filePath . '/' . $getItem->image);
                        }
                        Location::destroy($request->id);
                    }
                    return redirect()->route('route_admin.locations')->with('flash_message', 'Location removed!');
                    break;
            }
        } else {
            $data['lists'] = Location::with('projects')->get();
            $data['projects'] = Project::get();
            return view('backend.locations', $data);
        }
    }

    public function projects(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'projects';
        $data['active'] = 'projects';
        $data['sub_active'] = 'projects';
        $data['sub1_active'] = 'projects';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();
                        $insertData['alias'] = Str::slug($insertData['name'], '-');
                        if ($request->hasFile('photo')) {
                            $insertData['photo'] = Project::uploadImage($request, 'photo');
                        }
//                        dd($insertData);
                        Project::create($insertData);
                        return redirect()->route('route_admin.projects')->with('flash_message', 'Location Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Project::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $updateData['alias'] = Str::slug($updateData['name'], '-');
                            if ($request->hasFile('photo')) {
                                $updateData['photo'] = Project::uploadImage($request, 'photo', $getItem->photo);
                            }
                        }
                        $getItem->update($updateData);
                        return redirect()->route('route_admin.projects')->with('flash_message', 'Location updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Project::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        if ($getItem->image != '') {
                            $filePath = Project::uploadDir();
                            File::delete($filePath . '/' . $getItem->image);
                        }
                        Project::destroy($request->id);
                    }
                    return redirect()->route('route_admin.projects')->with('flash_message', 'Location removed!');
                    break;
            }
        } else {
            $data['lists'] = Project::get();
            return view('backend.projects', $data);
        }
    }

    public function successStories(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'successStories';
        $data['active'] = 'successStories';
        $data['sub_active'] = 'successStories';
        $data['sub1_active'] = 'successStories';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

                        SuccessStories::create($insertData);
                        return redirect()->route('route_admin.successStories')->with('flash_message', 'Success Stories Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = SuccessStories::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {

                            $getItem->update($updateData);
                        }
                        return redirect()->route('route_admin.successStories')->with('flash_message', 'Success Stories  updated!');
                    }
                    break;
                case 'remove':
                    $getItem = SuccessStories::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        SuccessStories::destroy($request->id);
                    }
                    return redirect()->route('route_admin.successStories')->with('flash_message', 'Success Stories  removed!');
                    break;
            }
        } else {
            $data['lists'] = SuccessStories::with('user')->get();
            $data['users'] = User::where('role', '!=', 404)->get();
            return view('backend.success_stories', $data);
        }
    }

    public function testimonials(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'testimonials';
        $data['active'] = 'testimonials';
        $data['sub_active'] = 'testimonials';
        $data['sub1_active'] = 'testimonials';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

                        Testimonials::create($insertData);
                        return redirect()->route('route_admin.testimonials')->with('flash_message', 'Success Stories Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Testimonials::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {

                            $getItem->update($updateData);
                        }
                        return redirect()->route('route_admin.testimonials')->with('flash_message', 'Success Stories  updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Testimonials::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        Testimonials::destroy($request->id);
                    }
                    return redirect()->route('route_admin.testimonials')->with('flash_message', 'Success Stories  removed!');
                    break;
            }
        } else {
            $data['lists'] = Testimonials::with('user')->get();
            $data['users'] = User::where('role', '!=', 404)->get();
            return view('backend.testimonials', $data);
        }
    }

    public function users(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'users';
        $data['active'] = 'users';
        $data['sub_active'] = 'users';
        $data['sub1_active'] = 'users';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {

                        $this->validate(request(), [
                            'name' => 'required',
                            'email' => 'required|email',
//                            'password' => 'required'
                        ]);

                        $insertData = $request->all();
                        $insertData['alias'] = Str::slug($insertData['name'], '-');
                        if ($request->hasFile('photo')) {
                            $insertData['photo'] = User::uploadImage($request, 'photo');
                        }
                        $insertData['password'] = Hash::make('123456789');
//                        dd($insertData);


                        User::create($insertData);
                        return redirect()->route('route_admin.users')->with('flash_message', 'Location Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = User::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $updateData['alias'] = Str::slug($updateData['name'], '-');
                            if ($request->hasFile('photo')) {
                                $updateData['photo'] = User::uploadImage($request, 'photo', $getItem->photo);
                            }
                        }
                        $getItem->update($updateData);
                        return redirect()->route('route_admin.users')->with('flash_message', 'Location updated!');
                    }
                    break;
                case 'remove':
                    $getItem = User::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        if ($getItem->image != '') {
                            $filePath = User::uploadDir();
                            File::delete($filePath . '/' . $getItem->image);
                        }
                        User::destroy($request->id);
                    }
                    return redirect()->route('route_admin.users')->with('flash_message', 'Location removed!');
                    break;
            }
        } else {
            $data['lists'] = User::where('role', '!=', 404)->get();
            return view('backend.users', $data);
        }
    }


    public function events(Request $request, $action = null)
    {


        $data = array();
        $data['title'] = 'events';
        $data['active'] = 'events';
        $data['sub_active'] = 'events';
        $data['sub1_active'] = 'events';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();


                        $events = new Event();

                        $events->alias = Str::slug($insertData['name'], '-');
                        if ($request->hasFile('photo')) {
                            $events->photo = Event::uploadImage($request, 'photo');
                        }
//                        dump($insertData);
//                        dd($insertData['sponsors_id']);


                        $events->name = $insertData['name'];
                        $events->description = $insertData['description'];
                        $events->event_start_date = Carbon::parse($insertData['event_start_date']);
                        $events->event_end_date = Carbon::parse($insertData['event_end_date']);
                        $events->location_id = $insertData['location_id'];
                        $events->project_id = $insertData['project_id'];
                        $events->status = $insertData['status'];


                        $events->save();

                        $events->sponsors()->sync($insertData['sponsorsData']);

//                        dd($events);

                        return redirect()->route('route_admin.events')->with('flash_message', 'Event Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Event::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $updateData['alias'] = Str::slug($updateData['name'], '-');
                            if ($request->hasFile('photo')) {
                                $updateData['photo'] = Event::uploadImage($request, 'photo', $getItem->photo);
                            }
                        }


                        $updateData['event_start_date'] = Carbon::parse($updateData['event_start_date']);
                        $updateData['event_end_date'] = Carbon::parse($updateData['event_end_date']);

                        $getItem->update($updateData);

                        $getItem->sponsors()->sync($updateData['sponsorsData']);

                        return redirect()->route('route_admin.events')->with('flash_message', 'Events updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Event::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        if ($getItem->photo != '') {
                            $filePath = Event::uploadDir();
                            File::delete($filePath . '/' . $getItem->photo);
                        }
                        Event::destroy($request->id);
                    }
                    return redirect()->route('route_admin.events')->with('flash_message', 'Events removed!');
                    break;
            }
        } else {

            $data['lists'] = Event::with('project', 'location', 'sponsors')->get();
            $data['locations'] = Location::where('status', 1)->get();
            $data['projects'] = Project::where('status', 1)->get();
            $data['sponsors'] = Sponsor::where('status', 1)->get();

//            dd($data['lists']);

            return view('backend.events', $data);
        }
    }


    public function financialsActivity(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'Activity';
        $data['active'] = 'Activity';
        $data['sub_active'] = 'Activity';
        $data['sub1_active'] = 'Activity';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

//                        dd($insertData);

                        FinancialsActivity::create($insertData);
                        return redirect()->route('route_admin.activity')->with('flash_message', 'activity Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = FinancialsActivity::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {

                            $getItem->update($updateData);
                        }
                        return redirect()->route('route_admin.activity')->with('flash_message', 'activity  updated!');
                    }
                    break;
                case 'remove':
                    $getItem = FinancialsActivity::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        FinancialsActivity::destroy($request->id);
                    }
                    return redirect()->route('route_admin.activity')->with('flash_message', 'activity  removed!');
                    break;
            }
        } else {
            $data['lists'] = FinancialsActivity::get();
            return view('backend.finacial.finacial_activity', $data);
        }
    }


    public function financialsActivityYearWise(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'Activity';
        $data['active'] = 'Activity';
        $data['sub_active'] = 'years';
        $data['sub1_active'] = 'years';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();


                        if ($request->hasFile('file_name')) {
                            $insertData['file_name'] = FinancialYear::uploadImage($request, 'file_name');
                        }

//                        dd($insertData);

                        FinancialYear::create($insertData);
                        return redirect()->route('route_admin.activity_yearwise')->with('flash_message', 'activity Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = FinancialYear::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {

                            $getItem->update($updateData);
                        }
                        return redirect()->route('route_admin.activity_yearwise')->with('flash_message', 'activity  updated!');
                    }
                    break;
                case 'remove':
                    $getItem = FinancialYear::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        FinancialYear::destroy($request->id);
                    }
                    return redirect()->route('route_admin.activity_yearwise')->with('flash_message', 'activity  removed!');
                    break;
            }
        } else {
            $data['lists'] = FinancialYear::get();
            return view('backend.finacial.finacial_years', $data);
        }
    }


    public function banners(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'banners';
        $data['active'] = 'banners';
        $data['sub_active'] = 'banners';
        $data['sub1_active'] = 'banners';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();
                        if ($request->hasFile('photo')) {
                            $insertData['photo'] = Banner::uploadImage($request, 'photo');
                        }
//                        dd($insertData);
                        Banner::create($insertData);
                        return redirect()->route('route_admin.banners')->with('flash_message', 'Banner Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Banner::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            if ($request->hasFile('photo')) {
                                $updateData['photo'] = Banner::uploadImage($request, 'photo', $getItem->photo);
                            }
                        }
                        $getItem->update($updateData);
                        return redirect()->route('route_admin.banners')->with('flash_message', 'Banner updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Banner::where('id', $request->id)->first();
                    if (!empty($getItem)) {
                        if ($getItem->photo != '') {
                            $filePath = Banner::uploadDir();
                            File::delete($filePath . '/' . $getItem->photo);
                        }
                        Banner::destroy($request->id);
                    }
                    return redirect()->route('route_admin.banners')->with('flash_message', 'Banner removed!');
                    break;
            }
        } else {
            $data['lists'] = Banner::get();
            return view('backend.banners', $data);
        }
    }


    public function contactUs(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'contactUs';
        $data['active'] = 'contactUs';
        $data['sub_active'] = 'contactUs';
        $data['sub1_active'] = 'contactUs';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

//                        dd($insertData);
                        Conatctus::create($insertData);
                        return redirect()->route('route_admin.contactUs')->with('flash_message', 'contactUs Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Conatctus::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $getItem->update($updateData);
                        }

                        return redirect()->route('route_admin.contactUs')->with('flash_message', 'contactUs updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Conatctus::where('id', $request->id)->first();
                    if (!empty($getItem)) {

                        Conatctus::destroy($request->id);
                    }
                    return redirect()->route('route_admin.contactUs')->with('flash_message', 'contactUs removed!');
                    break;
            }
        } else {
            $data['lists'] = Conatctus::get();
            return view('backend.conatctus', $data);
        }
    }


    public function joinAsVolunteer(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'contactUs';
        $data['active'] = 'contactUs';
        $data['sub_active'] = 'contactUs';
        $data['sub1_active'] = 'contactUs';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

//                        dd($insertData);
                        RegisterValunteer::create($insertData);
                        return redirect()->route('route_admin.joinAsVolunteer')->with('flash_message', 'Register Valunteer Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = RegisterValunteer::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $getItem->update($updateData);
                        }

                        return redirect()->route('route_admin.joinAsVolunteer')->with('flash_message', 'Register Valunteer updated!');
                    }
                    break;
                case 'remove':
                    $getItem = RegisterValunteer::where('id', $request->id)->first();
                    if (!empty($getItem)) {

                        RegisterValunteer::destroy($request->id);
                    }
                    return redirect()->route('route_admin.contactUs')->with('flash_message', 'Register Valunteer removed!');
                    break;
            }
        } else {
            $data['lists'] = RegisterValunteer::get();
            return view('backend.joinvalunter', $data);
        }
    }


    public function settings(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'settings';
        $data['active'] = 'settings';
        $data['sub_active'] = 'settings';
        $data['sub1_active'] = 'settings';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

//                        dd($insertData);
                        Setting::create($insertData);
                        return redirect()->route('route_admin.settings')->with('flash_message', 'settings Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = Setting::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $getItem->update($updateData);
                        }

                        return redirect()->route('route_admin.settings')->with('flash_message', 'settings updated!');
                    }
                    break;
                case 'remove':
                    $getItem = Setting::where('id', $request->id)->first();
                    if (!empty($getItem)) {

                        Setting::destroy($request->id);
                    }
                    return redirect()->route('route_admin.settings')->with('flash_message', 'settings removed!');
                    break;
            }
        } else {
            $data['lists'] = Setting::get();
            return view('backend.settings', $data);
        }
    }

    public function mediaVidoes(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'Media';
        $data['active'] = 'media';
        $data['sub_active'] = 'video';
        $data['sub1_active'] = 'video';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

//                        dd($insertData);
                        MediaVideo::create($insertData);
                        return redirect()->route('route_admin.media.vidoes')->with('flash_message', 'Media Video Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = MediaVideo::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $getItem->update($updateData);
                        }

                        return redirect()->route('route_admin.media.vidoes')->with('flash_message', 'Media Video updated!');
                    }
                    break;
                case 'remove':
                    $getItem = MediaVideo::where('id', $request->id)->first();
                    if (!empty($getItem)) {

                        MediaVideo::destroy($request->id);
                    }
                    return redirect()->route('route_admin.media.vidoes')->with('flash_message', 'Media Video removed!');
                    break;
            }
        } else {
            $data['lists'] = MediaVideo::get();
            return view('backend.media.vidoes', $data);
        }
    }


    public function mediaPress(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'Media';
        $data['active'] = 'media';
        $data['sub_active'] = 'press';
        $data['sub1_active'] = 'press';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();
                        if ($request->hasFile('photo')) {
                            $insertData['photo'] = MediaPress::uploadImage($request, 'photo');
                        }
//                        dd($insertData);
                        MediaPress::create($insertData);
                        return redirect()->route('route_admin.media.press')->with('flash_message', 'Media Press Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = MediaPress::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {


                            if ($request->hasFile('photo')) {
                                $updateData['photo'] = MediaPress::uploadImage($request, 'photo', $getItem->photo);
                            }

                            $getItem->update($updateData);
                        }


                        return redirect()->route('route_admin.media.press')->with('flash_message', 'Media Press updated!');
                    }
                    break;
                case 'remove':
                    $getItem = MediaPress::where('id', $request->id)->first();
                    if (!empty($getItem)) {

                        if ($getItem->photo != '') {
                            $filePath = MediaPress::uploadDir();
                            File::delete($filePath . '/' . $getItem->photo);
                        }
                        MediaPress::destroy($request->id);
                    }
                    return redirect()->route('route_admin.media.press')->with('flash_message', 'Media Press removed!');
                    break;
            }
        } else {
            $data['lists'] = MediaPress::get();
            return view('backend.media.press', $data);
        }
    }


    public function mediaGallery(Request $request, $action = null)
    {
        $data = array();
        $data['title'] = 'Media';
        $data['active'] = 'media';
        $data['sub_active'] = 'gallery';
        $data['sub1_active'] = 'gallery';
        if ($request->isMethod('post')) {
            $action = $request->action;
            switch ($action) {
                case 'add':
//                    dd('add');
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();

//                        dd($insertData);
                        MediaGallery::create($insertData);
                        return redirect()->route('route_admin.media.gallery')->with('flash_message', 'Media gallery Added!');
                    }
                    break;
                case 'edit':
                    if ($request->isMethod('post')) {
                        $updateData = $request->all();
                        $getItem = MediaGallery::where('id', $updateData['id'])->first();
                        if (!empty($getItem)) {
                            $getItem->update($updateData);
                        }

                        return redirect()->route('route_admin.media.gallery')->with('flash_message', 'Media gallery updated!');
                    }
                    break;
                case 'imagesAdd':
                    if ($request->isMethod('post')) {
                        $insertData = $request->all();
                        $getItem = MediaGallery::where('id', $insertData['id'])->first();
                        if (!empty($getItem)) {
                            $insertData['photo'] = MediaGalleryImages::uploadImage($request, 'photo');


                            if (count($insertData['photo']) > 0) {
                                foreach ($insertData['photo'] AS $photos) {

                                    $imagedata = array();
                                    $imagedata['gallery_id'] = $insertData['id'];
                                    $imagedata['image_name'] = $photos;

                                    MediaGalleryImages::create($imagedata);

                                }
                            }


                        }

                        return redirect()->route('route_admin.media.gallery')->with('flash_message', 'Media gallery updated!');
                    }
                    break;
                case 'remove':
                    $getItem = MediaGallery::where('id', $request->id)->first();
                    if (!empty($getItem)) {

                        MediaGallery::destroy($request->id);
                    }
                    return redirect()->route('route_admin.media.gallery')->with('flash_message', 'Media gallery removed!');
                    break;
                case 'imageremove':
                    $actionData = $request->all();
//                    dd($insertData);

                    $getItem = MediaGalleryImages::where('id', $actionData['imageid'])->first();
                    if (!empty($getItem)) {

                        if ($getItem->image_name != '') {
                            $filePath = MediaGalleryImages::uploadDir();
                            File::delete($filePath . '/' . $getItem->image_name);
                        }

                        MediaGalleryImages::destroy($actionData['imageid']);
                    }
                    return redirect()->route('route_admin.media.gallery')->with('flash_message', 'Media gallery removed!');
                    break;
            }
        } else {
            $data['lists'] = MediaGallery::get();
            return view('backend.media.gallery', $data);
        }
    }


}
