<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Conatctus;
use App\Models\Event;
use App\Models\FinancialsActivity;
use App\Models\FinancialsActivityInterest;
use App\Models\FinancialYear;
use App\Models\Location;
use App\Models\MediaGallery;
use App\Models\MediaPress;
use App\Models\MediaVideo;
use App\Models\Page;
use App\Models\Project;
use App\Models\RegisterValunteer;
use App\Models\Sponsor;
use App\Models\SuccessStories;
use App\Models\Testimonials;
use App\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Str;

class FrontendController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function home()
    {

        $data = array();
        $data['active'] = 'home';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = 'home';
        $data['meta_desc'] = 'home';
        $data['meta_keywords'] = 'home';
        $data['title'] = 'Home | [I & Eye, Sharing Vision]';

        $data['banners'] = Banner::where('status', 1)->get();

        $data['pageTypes'] = Page::with('pageTypes')->where('page_type_id', 2)->get();

        $data['gallery_lists'] = MediaGallery::with('galleryImages')->where('status', 1)->get();

        return view('site.home', $data);
    }

    public function aboutus(Request $request)
    {

        $data = array();
        $data['active'] = 'aboutus';
        $data['title'] = 'About Us |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';
        $data['pageinfo'] = Page::with('pageTypes')->where('page_type_id', 1)->where('alias', 'about-us')->first();


        return view('site.aboutus', $data);
    }

    public function newsroom()
    {

        $data = array();
        $data['active'] = 'newsroom';
        $data['title'] = 'News Room |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';

        $data['lists'] = MediaPress::where('status', 1)->get();


        return view('site.news_room', $data);
    }

    public function mediaGallry($album = '')
    {

        $data = array();
        $data['active'] = 'mediaGallry';
        $data['title'] = 'Gallery | Media |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';

        if ($album != '') {
            $data['galleryimages'] = MediaGallery::with('galleryImages')->where('album_name', $album)->where('status', 1)->first();
            return view('site.media.gallery_images', $data);
        } else {
            $data['lists'] = MediaGallery::with('galleryImages')->where('status', 1)->get();
            return view('site.media.gallery', $data);
        }


    }

    public function mediaVidoes()
    {

        $data = array();
        $data['active'] = 'mediaVidoes';
        $data['title'] = 'Vidoes | Media |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';
        $data['lists'] = MediaVideo::where('status', 1)->get();
        return view('site.media.videos', $data);
    }

    public function joinAsVolunteer(Request $request)
    {

        $data = array();
        $data['active'] = 'join_as_volunteer';
        $data['title'] = 'Join As Volunteer |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';


        if ($request->isMethod('post')) {


            $this->validate(request(), [
                'form_name' => 'required',
                'form_email' => 'required|email',
                'form_subject' => 'required',
                'form_phone' => 'required',
                'form_message' => 'required',
//                'g-recaptcha-response' => 'required|recaptcha'
            ], [
                'form_name.required' => 'Name Field is Required',
                'form_email.required' => 'Email Field is Required',
                'form_email.email' => 'Email must be a valid email address',
                'form_subject.required' => 'Subject is Required',
                'form_phone.required' => 'Contact Number Field is Required',
                'form_message.required' => 'Message is Required',
//                'g-recaptcha-response.required' => 'recaptcha is Required',
//                'g-recaptcha-response.recaptcha' => 'recaptcha is invalid'
            ]);


            $conatctdata = $request->all();

            RegisterValunteer::create($conatctdata);

            return redirect()->route('site.join_as_volunteer')->with('flash_message', 'Conatct Submited!');


        }

        return view('site.join_as_volunteer', $data);
    }

    public function faq()
    {

        $data = array();
        $data['active'] = 'faq';
        $data['title'] = 'FAQ |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';
        $data['pageinfo'] = Page::with('pageTypes')->where('page_type_id', 1)->where('alias', 'faq')->first();
        return view('site.faq', $data);
    }

    public function contactus(Request $request)
    {

        $data = array();
        $data['active'] = 'contactus';
        $data['title'] = 'Conatct Us |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
        $data['sub2_active'] = '';
        $data['meta_title'] = '';
        $data['meta_desc'] = '';
        $data['meta_keywords'] = '';


        if ($request->isMethod('post')) {


            $this->validate(request(), [
                'form_name' => 'required',
                'form_email' => 'required|email',
                'form_subject' => 'required',
                'form_phone' => 'required',
                'form_message' => 'required',
//                'g-recaptcha-response' => 'required|recaptcha'
            ], [
                'form_name.required' => 'Name Field is Required',
                'form_email.required' => 'Email Field is Required',
                'form_email.email' => 'Email must be a valid email address',
                'form_subject.required' => 'Subject is Required',
                'form_phone.required' => 'Contact Number Field is Required',
                'form_message.required' => 'Message is Required',
//                'g-recaptcha-response.required' => 'recaptcha is Required',
//                'g-recaptcha-response.recaptcha' => 'recaptcha is invalid'
            ]);


            $conatctdata = $request->all();

            Conatctus::create($conatctdata);

            return redirect()->route('site.contactus')->with('flash_message', 'Conatct Submited!');


        }

        return view('site.contactus', $data);
    }


    public function initiative($alias)
    {

        $data = array();

        $data['pageinfo'] = Page::with('pageTypes')->where('page_type_id', 2)->where('alias', $alias)->first();

//        dd($data['pageinfo']);


        $data['active'] = $data['pageinfo']->pageTypes->name;
        $data['title'] = $data['pageinfo']->title . ' |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = $alias;
        $data['sub2_active'] = $alias;
        $data['meta_title'] = $alias;
        $data['meta_desc'] = $alias;
        $data['meta_keywords'] = $alias;

        return view('site.initiative.pages', $data);
    }


    public function locations($alias = null)
    {

        $data = array();
        $data['active'] = 'locations';
        $data['title'] = 'Locations |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'locations';
        $data['meta_desc'] = 'locations';
        $data['meta_keywords'] = 'locations';

        if ($alias != '') {
            $data['info'] = Location::with('projects')->where('status', 1)->where('alias', $alias)->first();


            $data['title'] = $data['info']->name . '| Locations |  [I & Eye, Sharing Vision]';

            return view('site.initiative.locations_info', $data);
        } else {
            $data['lists'] = Location::with('projects')->where('status', 1)->get();

            return view('site.initiative.locations', $data);
        }


    }

    public function projects($alias = null)
    {

        $data = array();
        $data['active'] = 'projects';
        $data['title'] = 'Projects |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'projects';
        $data['meta_desc'] = 'projects';
        $data['meta_keywords'] = 'projects';


        if ($alias != '') {
            $data['info'] = Project::where('status', 1)->where('alias', $alias)->first();

            $data['title'] = $data['info']->name . '| Projects |  [I & Eye, Sharing Vision]';


            return view('site.initiative.projects_info', $data);
        } else {
            $data['lists'] = Project::where('status', 1)->get();

            return view('site.initiative.projects', $data);
        }


    }

    public function testimonials()
    {

        $data = array();
        $data['active'] = 'initiative';
        $data['title'] = 'Testimonials |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = 'testimonials';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'testimonials';
        $data['meta_desc'] = 'testimonials';
        $data['meta_keywords'] = 'testimonials';

        $data['lists'] = Testimonials::with('user')->where('status', 1)->get();

        return view('site.testimonials', $data);
    }

    public function events($name = null)
    {

        $data = array();
        $data['active'] = 'events';
        $data['title'] = 'Events |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = 'list';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'events';
        $data['meta_desc'] = 'events';
        $data['meta_keywords'] = 'events';

        if ($name != null) {

//            dd($name);

            $data['sub1_active'] = 'view';
            $data['info'] = Event::with('project', 'location', 'sponsors')->where('alias', $name)->where('status', 1)->first();

            $data['title'] = $data['info']->name . '| Events |  [I & Eye, Sharing Vision]';

            return view('site.events.view', $data);
        } else {

            $data['lists'] = Event::with('project', 'location', 'sponsors')->where('status', 1)->get();

//            dd($data['lists']);

            return view('site.events.lists', $data);
        }


    }

    public function category($name = null)
    {

        $data = array();
        $data['active'] = 'category';

        $data['sub1_active'] = $name;
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'category';
        $data['meta_desc'] = 'category';
        $data['meta_keywords'] = 'category';

        if ($name != null) {

            $data['title'] = ucwords(str_replace('-', ' ', $name)) . ' |  [I & Eye, Sharing Vision]';
//            dd(array_flip(userRoles()));

            $userRoles = userRoles();

//            dump($userRoles);

            $userRoles = array_map(function ($value) {
                return Str::slug($value, '-');
            }, $userRoles
            );

            $userRolesReverce = array_flip($userRoles);
//            dump($userRolesReverce);

            if (isset($userRolesReverce[$name])) {

                $userRoleId = $userRolesReverce[$name];
                $data['lists'] = User::where('status', 1)->where('role', $userRoleId)->get();


                return view('site.category.lists', $data);
            } else {
                $data['lists'] = User::where('status', 1)->where('role', '!=', 404)->get();


                return view('site.category.lists', $data);


            }


        } else {

            $data['lists'] = User::where('status', 1)->where('role', '!=', 404)->get();


            return view('site.category.lists', $data);
        }


    }

    public function successStories($name = null)
    {

        $data = array();
        $data['active'] = 'initiative';
        $data['title'] = 'Success Stories |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = 'success_stories';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'success_stories';
        $data['meta_desc'] = 'success_stories';
        $data['meta_keywords'] = 'success_stories';

        if ($name != null) {

            $data['sub1_active'] = 'view';

            return view('site.events.view', $data);
        } else {

            $data['lists'] = SuccessStories::with('user')->where('status', 1)->get();

            return view('site.success_stories.lists', $data);
        }


    }

    public function sponsors()
    {

        $data = array();
        $data['active'] = 'sponsors';
        $data['title'] = 'Sponsors |  [I & Eye, Sharing Vision]';
        $data['sub1_active'] = '';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'sponsors';
        $data['meta_desc'] = 'sponsors';
        $data['meta_keywords'] = 'sponsors';

        $data['lists'] = Sponsor::where('status', 1)->get();

        return view('site.sponsors', $data);
    }

    public function financialsCurrent()
    {

        $data = array();
        $data['active'] = 'financials';
        $data['sub1_active'] = 'current';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'current';
        $data['meta_desc'] = 'current';
        $data['meta_keywords'] = 'current';

        $data['lists'] = FinancialYear::where('status', 1)->get();

        return view('site.financials.current', $data);
    }

    public function financialsNextYear(Request $request)
    {

        $data = array();
        $data['active'] = 'financials';
        $data['sub1_active'] = 'nextyear';
//        $data['sub2_active'] = 'arogya';
        $data['meta_title'] = 'nextyear';
        $data['meta_desc'] = 'nextyear';
        $data['meta_keywords'] = 'nextyear';

        if ($request->isMethod('post')) {

            $requestData = $request->all();

//            dd($requestData);

            FinancialsActivityInterest::create($requestData);

            return redirect()->route('site.financials.nextyear')->with('flash_message', 'Activity Interest Added!');


        } else {
            $data['lists'] = FinancialsActivity::with('interests')->where('status', 1)->get();

            return view('site.financials.nextyear', $data);
        }


    }


}
