<?php

namespace App\Models;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class MediaGalleryImages extends Authenticatable
{
    use Notifiable;

    protected $table = 'media_gallery_images';

    protected $fillable = [
        'gallery_id',
        'image_name',
        'status',
    ];


    public static function uploadDir($url = null)
    {
        $folder = '/uploads/galleryimages';
        if ($url == 'url') {
            return url($folder . '/');
        } else {
            return public_path($folder);
        }
    }

    public static function uploadImage($request, $input, $oldImage = null)
    {
        $images = array();
        if ($request->hasFile($input)) {


            if ($files = $request->file($input)) {
                foreach ($files as $key=>$file) {

//                    $file = $request->file($input);
                    $uploadPath = self::uploadDir();
                    $extension = $file->getClientOriginalExtension();
                    $fileName = 'gallery_'.$key.'_' . time() . '.' . $extension;
                    $file->move($uploadPath, $fileName);

                    if ($oldImage != null) {
                        if ($oldImage != null && $fileName != '') {
                            File::delete($uploadPath . '/' . $oldImage);
                        }
                    }
                    $images [] = $fileName;

//                    dd($fileName);


                }
            }


        }

        return $images;
    }


    public function gallery()
    {
        return $this->belongsTo(MediaGallery::class, 'gallery_id','id');
    }

}
