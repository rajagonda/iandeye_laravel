<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class Page extends Authenticatable
{
    use Notifiable;

    protected $table = 'pages';

    protected $fillable = [
        'title', 'alias', 'page_type_id', 'description', 'photo', 'status'
    ];


    public function pageTypes()
    {
        return $this->belongsTo(PageType::class, 'page_type_id','id');
    }


    public static function uploadDir($url = null)
    {
        $folder = '/uploads/pages';
        if ($url == 'url') {
            return url($folder . '/');
        } else {
            return public_path($folder);
        }
    }

    public static function uploadImage($request, $input, $oldImage = null)
    {
        if ($request->hasFile($input)) {

            $file = $request->file($input);
            $uploadPath = self::uploadDir();
            $extension = $file->getClientOriginalExtension();
            $fileName = 'pages_' . time() . '.' . $extension;
            $file->move($uploadPath, $fileName);

            if ($oldImage != null) {
                if ($oldImage != null && $fileName != '') {
                    File::delete($uploadPath . '/' . $oldImage);
                }
            }
            return $fileName;
        }
    }


}
