<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class Event extends Authenticatable
{
    use Notifiable;

    protected $table = 'events';

    protected $fillable = [
        'name',
        'alias',
        'photo',
        'description',
        'event_start_date',
        'event_end_date',
        'location_id',
        'project_id',
        'status'
    ];


    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }

    public function sponsors()
    {
        return $this->belongsToMany(Sponsor::class, 'sponsors_events',  'events_id', 'sponsors_id');
    }


    public static function uploadDir($url = null)
    {
        $folder = '/uploads/events';
        if ($url == 'url') {
            return url($folder . '/');
        } else {
            return public_path($folder);
        }
    }

    public static function uploadImage($request, $input, $oldImage = null)
    {
        if ($request->hasFile($input)) {

            $file = $request->file($input);
            $uploadPath = self::uploadDir();
            $extension = $file->getClientOriginalExtension();
            $fileName = 'pages_' . time() . '.' . $extension;
            $file->move($uploadPath, $fileName);

            if ($oldImage != null) {
                if ($oldImage != null && $fileName != '') {
                    File::delete($uploadPath . '/' . $oldImage);
                }
            }
            return $fileName;
        }
    }


}
