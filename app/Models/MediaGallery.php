<?php

namespace App\Models;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class MediaGallery extends Authenticatable
{
    use Notifiable;

    protected $table = 'media_gallery';

    protected $fillable = [
        'album_name',
        'status',
    ];


    public function galleryImages()
    {
        return $this->hasMany(MediaGalleryImages::class, 'gallery_id','id');
    }


}
