<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class Sponsor extends Authenticatable
{
    use Notifiable;

    protected $table = 'sponsors';

    protected $fillable = [
        'name', 'alias', 'image', 'status',
    ];


    public static function uploadDir($url = null)
    {
        $folder = '/uploads/sponsors';
        if ($url == 'url') {
            return url($folder . '/');
        } else {
            return public_path($folder);
        }
    }

    public static function uploadImage($request, $input, $oldImage = null)
    {
        if ($request->hasFile($input)) {

            $file = $request->file($input);
            $uploadPath = self::uploadDir();
            $extension = $file->getClientOriginalExtension();
            $fileName = 'sponsors_' . time() . '.' . $extension;
            $file->move($uploadPath, $fileName);

           if ($oldImage != null) {
                if ($oldImage != null && $fileName != '') {
                    File::delete($uploadPath . '/' . $oldImage);
                }
            }
            return $fileName;
        }
    }

}
