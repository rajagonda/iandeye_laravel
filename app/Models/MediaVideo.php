<?php

namespace App\Models;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class MediaVideo extends Authenticatable
{
    use Notifiable;

//    protected $table = 'testimonials';

    protected $fillable = [
        'title', 'video', 'status',
    ];




}
