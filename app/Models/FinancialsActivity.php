<?php

namespace App\Models;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class FinancialsActivity extends Authenticatable
{
    use Notifiable;

    protected $table = 'activity';

    protected $fillable = [
        'activity_name', 'description', 'period', 'no_of_beneficiaries', 'project_cost', 'status', 'year'
    ];

    public function interests()
    {
        return $this->hasMany(FinancialsActivityInterest::class, 'activity_id', 'id');
    }

}
