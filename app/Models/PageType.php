<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class PageType extends Authenticatable
{
    use Notifiable;

    protected $table = 'page_type';

    protected $fillable = [
        'name', 'alias', 'photo', 'status',
    ];


    public function pages()
    {
        return $this->hasMany(Page::class, 'id', 'page_type_id');
    }


}
