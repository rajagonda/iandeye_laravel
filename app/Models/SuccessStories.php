<?php

namespace App\Models;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class SuccessStories extends Authenticatable
{
    use Notifiable;

    protected $table = 'success_stories';

    protected $fillable = [
        'users_id', 'description', 'status',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }


}
