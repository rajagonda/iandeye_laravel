<?php

namespace App\Models;

use App\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class FinancialsActivityInterest extends Authenticatable
{
    use Notifiable;

    protected $table = 'activity_interest';

    protected $fillable = [
        'activity_id', 'name', 'email', 'phone', 'price', 'status',
    ];


}
