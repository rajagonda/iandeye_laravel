<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\File;

class Conatctus extends Authenticatable
{
    use Notifiable;

    protected $table = 'conatctus';

    protected $fillable = [
        'form_name', 'form_email', 'form_subject', 'form_phone', 'form_message', 'status',
    ];


}
