<?php

use App\Models\Categories;
use App\Models\Page;
use App\Models\PageViews;
use App\Models\Posts;

use Carbon\Carbon;
use Jenssegers\Agent\Agent;


define('ADMIN_EMAIL', 'info@idconnect.com');


function metaData($data = null)
{
    $array = array(
        'title' => 'IdreamPost | News Web Portal for Political News | Movie Updates | Interviews',
        'meta_keywords' => 'iDreamPost.com, videos, Tollywood Movie Updates, Politics News, Show Times, Movie Release Dates, Todays News, Reviews, Movie Gallery, Breaking News, Tollywood News',
        'meta_description' => 'IdreamPost | News Web Portal for Political News | Movie Updates | Interviews',
        'meta_image' => '',

    );


    if ($data != null) {
        if (isset($array[$data])) {
            return $array[$data];
        } else {
            return '';
        }
    } else {
        return $array;
    }


}


function form_flash_message($name = null)
{


    if ($name == null) {
        $name = "flash_message";
    }

    if (\Illuminate\Support\Facades\Session::has($name)) {
        ?>
        <br/>
        <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong><?= Session::get($name) ?></strong>
        </div>
        <?php
    }

}

function initiativeList()
{
    return Page::with('pageTypes')->where('page_type_id', 2)->get();
}

function settings($name)
{
    $data = \App\Models\Setting::first();

    if ($name != '') {
        return $data->$name;
    }


}

function status_list($status = '')
{
    $array = array(
        '1' => 'Active',
        '2' => 'Block',
        '3' => 'Schedule',
        '4' => 'Draft',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}

function activity_list($status = '')
{
    $array = array(
        '1' => 'Open',
        '2' => 'Close',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }
}


function userRoles($status = '')
{
    $array = array(
//        '404' => 'admin',
//        '0' => 'Others',
        '1' => 'Old & Current Students',
        '2' => 'School/college Teachers & Management',
        '3' => 'Volunteers',
        '4' => 'Sponsors',
        '5' => 'Employees',
    );

    if ($status === '') {
        return $array;
    } else {
        if (isset($array[$status])) {
            return $array[$status];
        } else {
            return '';
        }
    }


}


function imageNotAvalableUrl()
{
    return 'https://dummyimage.com/600x400/FFF/999999&text=image+not+available';
}


function form_validation_error($errors, $name)
{

    if ($errors->has($name)) {
        ?>
        <span class="text-danger">
            <?= $errors->first($name) ?>
        </span>
        <?php
    }


}

